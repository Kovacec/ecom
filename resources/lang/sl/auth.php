<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'login' => 'Prijava',
    'email' => 'Elektronska pošta',
    'password' => 'Geslo',
    'remember_me' => 'Zapomni si me',
    'forgot_your_password' => 'Ste pozabili geslo?',
    'submit' => 'Prijava',
    'failed' => 'Neveljavno uporabniško ime.',
    'throttle' => 'Preveč poiskusov. Prosimo poiskusite ponovno čez :seconds sekund.',
];
