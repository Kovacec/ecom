<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various meta information about
    | messages that we need to display to the user.
    |
    | Contact Form
    |
    */

    'message.from' => 'Od',
    'message.body' => 'Vsebina',

];