@extends('layouts.app')


@section('content')
    <div class="section">
        <div class="container">

            <h1 class="title">Create item for {{ $entity->name }}</h1>


            <form action="/item" method="POST" enctype="multipart/form-data">

                {{ csrf_field() }}

                {{-- Each product has to belong to some entity that will take care of shipping --}}
                <input type="hidden" name="entity" value="{{ $entity->id }}">

                <div class="columns box">

                    <div class="column is-half">

                        <div class="field">
                            <label for="name" class="label">Name</label>
                            <input type="text"
                                   class="input{{ $errors->has('name') ? ' is-danger' : '' }}"
                                   id="name"
                                   value="{{ old('name') }}"
                                   name="name">

                            @if ($errors->has('name'))
                                <p class="help is-danger">
                                    {{ $errors->first('name') }}
                                </p>
                            @endif
                        </div>

                        {{--
                        TODO: autocomplete / with parent_id = 0
                        <div class="field">
                            <label for="category" class="label">Category</label>
                            <input type="text" class="input" id="category" name="category">
                        </div>
                        --}}

                        <div class="field">
                            <label for="description" class="label">Description</label>
                            <textarea name="description" id="description" class="textarea"
                                      rows="10">{{ old('description') }}</textarea>
                        </div>

                        <div class="field">
                            <label for="image" class="label">Logo:</label>
                            <input type="file"
                                   name="image"
                                   id="image"
                                   value="{{ old('image') }}"
                                   class="file">
                        </div>

                    </div>

                    <div class="column is-half">


                        {{--
                        <div class="field">
                            <label for="ean" class="label">Type:</label>
                            <p class="control has-icons-left">
                                <input type="text" class="input" id="ean" name="ean" placeholder="EAN">
                                <span class="icon is-small is-left">
                                <i class="fa fa-anchor"></i>
                            </span>
                            </p>
                        </div>
                        --}}

                        <div class="field">
                            <label for="type" class="label">Type:</label>
                            <p class="control has-icons-left">
                                <span class="select is-fullwidth">
                                    <select id="type" name="type">

                                        <?php // xdebug_break(); ?>


                                        @foreach(\App\Com\ItemType::values as $value)
                                            <option @if ($value == \App\Com\ItemType::GOODS)
                                                        selected
                                                    @endif value="goods">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </span>
                                <span class="icon is-small is-left">
                                <i class="fa fa-globe"></i>
                                </span>
                            </p>
                        </div>

                        <div class="field">
                            <label for="ean" class="label">EAN:</label>
                            <p class="control has-icons-left">
                                <input type="text"
                                       class="input"
                                       id="ean"
                                       name="ean"
                                       value="{{ old('ean') }}"
                                       placeholder="EAN">
                                <span class="icon is-small is-left">
                                <i class="fa fa-anchor"></i>
                            </span>
                            </p>


                            @if ($errors->has('ean'))
                                <p class="help is-danger">
                                    {{ $errors->first('ean') }}
                                </p>
                            @endif
                        </div>

                        <div class="field">
                            <label for="price" class="label">Price [including VAT]:</label>
                            <p class="control has-icons-left">
                                <input type="text"
                                       class="input{{ $errors->has('price') ? ' is-danger' : '' }}"
                                       id="price"
                                       name="price"
                                       value="{{ old('price') }}"
                                       placeholder="Price">
                                <span class="icon is-small is-left">
                                <i class="fa fa-euro"></i>
                            </span>
                            </p>

                            @if ($errors->has('price'))
                                <p class="help is-danger">
                                    {{ $errors->first('price') }}
                                </p>
                            @endif
                        </div>

                        <div class="field">
                            <label for="vat" class="label">VAT [%]:</label>
                            <p class="control has-icons-left">
                                <input type="text"
                                       class="input{{ $errors->has('vat') ? ' is-danger' : '' }}"
                                       id="vat"
                                       name="vat"
                                       value="{{ old('vat') }}"
                                       placeholder="VAT">
                                <span class="icon is-small is-left">
                                <i class="fa fa-tag"></i>
                            </span>
                            </p>

                            @if ($errors->has('vat'))
                                <p class="help is-danger">
                                    {{ $errors->first('vat') }}
                                </p>
                            @endif
                        </div>

                        <div class="field">
                            <label for="weight" class="label">Weight [g]:</label>
                            <p class="control has-icons-left">
                                <input type="text"
                                       class="input{{ $errors->has('weight') ? ' is-danger' : '' }}"
                                       id="weight"
                                       name="weight"
                                       value="{{ old('weight') }}"
                                       placeholder="Weight">
                                <span class="icon is-small is-left">
                                <i class="fa fa-tag"></i>
                            </span>
                            </p>

                            @if ($errors->has('weight'))
                                <p class="help is-danger">
                                    {{ $errors->first('weight') }}
                                </p>
                            @endif
                        </div>

                        <div class="field">
                            <label for="stock" class="label">Stock:</label>
                            <p class="control has-icons-left">
                                <input type="text"
                                       class="input"
                                       id="stock"
                                       name="stock"
                                       value="{{ old('stock') }}"
                                       placeholder="Stock">
                                <span class="icon is-small is-left">
                                <i class="fa fa-dot-circle-o"></i>
                            </span>
                            </p>
                            @if ($errors->has('stock'))
                                <p class="help is-danger">
                                    {{ $errors->first('stock') }}
                                </p>
                            @endif
                        </div>


                    </div>



                </div>

                <div class="field">
                    <div class="control">
                        <button type="submit" class="button is-primary">Create</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection