<div class="tags">
    @if(count($item->tags))
        @foreach($item->tags as $tag)
            <span class="tag"><a href="/items/tags/{{ $tag->name }}">{{ $tag->name }}</a></span>
        @endforeach
    @endif
</div>
