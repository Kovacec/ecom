<form action="/item/{{ $item->id }}" method="post" enctype="multipart/form-data">

    {{ method_field('PATCH') }}
    {{ csrf_field() }}

    <div class="columns box">

        <div class="column is-half">

            <div class="field">
                <label for="name" class="label">Name</label>
                <input type="text"
                       class="input{{ $errors->has('name') ? ' is-danger' : '' }}"
                       id="name"
                       value="{{ $item->name }}"
                       name="name">

                @if ($errors->has('name'))
                <p class="help is-danger">
                    {{ $errors->first('name') }}
                </p>
                @endif
            </div>

            {{--
            TODO: autocomplete / with parent_id = 0
            <div class="field">
                <label for="category" class="label">Category</label>
                <input type="text" class="input" id="category" name="category">
            </div>
            --}}

            <div class="field">
                <label for="description" class="label">Description</label>
                <textarea name="description" id="description" class="textarea"
                          rows="10">{{ $item->description }}</textarea>
            </div>


            <div class="field">
                <label for="image" class="label">Image:</label>
                <input type="file"
                       name="image"
                       id="image"
                       class="file">

                @if($item->logo)
                <img src="{{ Storage::disk('s3')->url($item->image) }}" alt=""
                     style="width: 100%">
                @endif

            </div>

        </div>

        <div class="column is-half">

            {{--
            <div class="field">
                <label for="ean" class="label">Type:</label>
                <p class="control has-icons-left">
                    <input type="text" class="input" id="ean" name="ean" placeholder="EAN">
                    <span class="icon is-small is-left">
                    <i class="fa fa-anchor"></i>
                </span>
                </p>
            </div>
            --}}

            <div class="field">
                <label for="type" class="label">Type:</label>
                <p class="control has-icons-left">

                                <span class="select is-fullwidth">
                                    <select id="type" name="type">

                                        <option
                                                @if ($item->type == \App\Com\ItemType::GOODS)
                                                selected
                                                @endif
                                                value="goods">goods</option>
                                    </select>
                                </span>

                    <span class="icon is-small is-left">
                                <i class="fa fa-globe"></i>
                                </span>
                </p>
            </div>

            <div class="field">
                <label for="ean" class="label">EAN:</label>
                <p class="control has-icons-left">
                    <input type="text"
                           class="input"
                           id="ean"
                           name="ean"
                           value="{{ $item->EAN }}"
                           placeholder="EAN">
                    <span class="icon is-small is-left">
                                <i class="fa fa-anchor"></i>
                            </span>
                </p>


                @if ($errors->has('ean'))
                <p class="help is-danger">
                    {{ $errors->first('ean') }}
                </p>
                @endif
            </div>

            <div class="field">
                <label for="price" class="label">Price [including VAT]:</label>
                <p class="control has-icons-left">
                    <input type="text"
                           class="input{{ $errors->has('price') ? ' is-danger' : '' }}"
                           id="price"
                           name="price"
                           value="{{ $item->price }}"
                           placeholder="Price">
                    <span class="icon is-small is-left">
                                <i class="fa fa-euro"></i>
                            </span>
                </p>

                @if ($errors->has('price'))
                <p class="help is-danger">
                    {{ $errors->first('price') }}
                </p>
                @endif
            </div>

            <div class="field">
                <label for="vat" class="label">VAT [%]:</label>
                <p class="control has-icons-left">
                    <input type="text"
                           class="input{{ $errors->has('vat') ? ' is-danger' : '' }}"
                           id="vat"
                           name="vat"
                           value="{{ $item->vat }}"
                           placeholder="VAT">
                    <span class="icon is-small is-left">
                                <i class="fa fa-tag"></i>
                            </span>
                </p>

                @if ($errors->has('vat'))
                <p class="help is-danger">
                    {{ $errors->first('vat') }}
                </p>
                @endif
            </div>

            <div class="field">
                <label for="weight" class="label">Weight [g]:</label>
                <p class="control has-icons-left">
                    <input type="text"
                           class="input{{ $errors->has('weight') ? ' is-danger' : '' }}"
                           id="weight"
                           name="weight"
                           value="{{ $item->weight }}"
                           placeholder="Weight">
                    <span class="icon is-small is-left">
                                <i class="fa fa-tag"></i>
                            </span>
                </p>

                @if ($errors->has('weight'))
                <p class="help is-danger">
                    {{ $errors->first('weight') }}
                </p>
                @endif
            </div>

            <div class="field">
                <label for="stock" class="label">Stock:</label>
                <p class="control has-icons-left">
                    <input type="text"
                           class="input"
                           id="stock"
                           name="stock"
                           value="{{ $item->stock }}"
                           placeholder="Stock">
                    <span class="icon is-small is-left">
                                <i class="fa fa-dot-circle-o"></i>
                            </span>
                </p>
                @if ($errors->has('stock'))
                <p class="help is-danger">
                    {{ $errors->first('stock') }}
                </p>
                @endif
            </div>


        </div>

    </div>
    <div>
        <div class="field">
            <div class="control">
                <button type="submit" class="button is-primary">Update</button>
            </div>
        </div>
    </div>

</form>