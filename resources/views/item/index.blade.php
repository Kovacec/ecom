@extends('layouts.app')

@section('content')

    <h1>test</h1>
    <div class="columns">
        <div class="column">

            <div class="field">
                <p class="control has-icons-left">
                    <input class="input"
                           type="text"
                           placeholder="Filter"
                           v-model="itemsFilter"
                    @keyUp="filterItems">


                    <span class="icon is-small is-left">
                            <i class="fa fa-search"></i>
                        </span>
                </p>

            </div>

        </div>

        <div class="column is-2">
            <a href="" class="button">
                <i class="fa fa-search"></i>
            </a>
        </div>

    </div>

    <items></items>


@endsection

@section('scripts')
    <script>
        import app from '/js/app.js'
        console.log(app)
    </script>
@endsection