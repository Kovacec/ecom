@extends('layouts.app')


@section('content')

    <section class="hero is-primary">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                    {{ $item->name }}
                </h1>
                <h2 class="subtitle">
                    Producer page
                </h2>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="columns">
                @if($item->image)
                    <div class="column is-one-quarter">
                        <figure class="image is-3by4">
                            <img src="{{ Storage::disk('s3')->url($item->image)  }}">
                        </figure>
                    </div>
                @endif

                <div class="column">
                    <p>
                        {!! $item->description !!}
                    </p>

                    <div class="buttons has-addons">

                        <a href="/cart/add" class="button is-primary"
                           @click.prevent="addToCart({{ json_encode($item) }})">
                            <span class="icon">
                                <i aria-hidden="true" class="fa fa-shopping-basket"></i>
                            </span>
                            <span>{{ trans("item.show.cart.add") }} :<b> {{ $item->price }} EUR </b></span>
                        </a>

                        {{--
                        @if($item->belongsToUser(Auth::user()))
                            <a href="/item/edit/{{ $item->id }}" class="button is-info">Edit: DRAFT</a>
                            <a href="/item/{{ $item->id }}" class="button is-danger">Delete</a>
                        @endif
                        --}}

                    </div>
                </div>

            </div>
        </div>
    </section>


    {{--
    @if($item->category_id)
        <section class="section">
            <div class="container">
                <div class="notification">
                    <p>{{ trans('item.show.category') }}: <a href="/items/category/"
                                                             rel="category={{ $item->category->id }}"
                                                             @click.prevent="queryItems">{{ $item->category->name }}</a>
                    </p>
                </div>
            </div>
        </section>
    @endif
    --}}

    @if(!empty($reviews))
        <section class="section">
            <div class="container">
                <div class="notification">
                    <p class="subtitle">item.show.reviews</p>
                    <p>item.show.reviews.no-reviews</p>
                </div>
            </div>
        </section>
    @endif


    <section class="section">
        <div class="container">
            <div class="notification">
                <p class="subtitle">item.show.ask.question.ttl</p>
                <p>ask.question.no-answers</p>
                <p class="has-text-right">
                    <a href="#"
                       class="button is-warning is-inverted"
                       rel="{{ $item->id }}"
                       @click.prevent="itemShowContact">item.show.ask-question</a>
                </p>
            </div>
        </div>
    </section>

    @include('message.modal', compact('item'))


    @if(!empty($tags) && count($tags))
        <section class="section">
            <div class="container">
                <p class="subtitle">item.show.tags.ttl</p>
                @include('item.partials.tags')
            </div>
        </section>
    @endif

    <section class="section">
        <div class="container">
            <p class="subtitle">item.show.comments</p>
            <div id="disqus_thread"></div>
        </div>
    </section>

    {{--
    @if($categoryItems)
        <div class="section">
            <div class="container">
                <div class="main-carousel"
                     data-flickity='{ "cellAlign": "left", "contain": true, "groupCells": true, "pageDots": false }'>
                    @foreach($categoryItems as $cItem)
                        <div class="carousel-cell">
                            <a href="/item/{{ $cItem->id }}">
                                <img src="{{ Storage::disk('s3')->url($cItem->image)  }}" alt="">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    --}}



@endsection


@section('scripts')
    <script>

        /**
         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
        /*
         var disqus_config = function () {
         this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
         this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
         };
         */



        /*
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = '//bojan-kovacec-net.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
        */
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by
            Disqus.</a></noscript>

@endsection