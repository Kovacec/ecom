@extends('layouts.app')

<?php
// dd();
?>

@section('content')

    <div class="section">
        <div class="container">

            <h1 class="title">Edit item</h1>

            @include('item.partials.edit-form')


        </div>
    </div>


@endsection