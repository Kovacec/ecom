Pozdravljeni,

kliknite na spodnjo povezavo za potrditev prijave na elektroneske novice:

{{ url('/newsletter/confirm') }}/{{ $newsletter->token }}



Od elektronskih novic se vedno odjavite na povezavi: {{ url('/newsletter/unsubscribe') }}

Lep pozdrav,
ekipa ekoloski-izdelki.si

