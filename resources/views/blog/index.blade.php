@extends('layouts.app')

@section('content')

    <section class="section">
        <div class="container">

            <br><br>

            <h1 class="title">System Architecture</h1>

            <p>How to host web space serverless and still use standard frameworks that run on EC2 instances for content creation and business logic. </p>

            <p>We use AWS <a href="https://aws.amazon.com/about-aws/global-infrastructure/">global infrastructure</a>.</p>
            <br>

            <p>http://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html : "The website is then available at the AWS Region-specific website endpoint of the bucket"</p>

            <br>
            <p><a href="https://github.com/vuejs/vuex">vuex</a>.</p>
            <!-- More in Video -->
            <p>Challange: time to content: <a href="https://streamacon.com/video/laracon-us-2017/day-1-evan-you">Even You: vuejs - native rendering, unit testing, time to content, shared nodejs</a></p>


            <br><br>
            <h2 class="subtitle">Serverless</h2>
            <p>Page is served through serveless application layer for public access > API Gateway - Lambda - RDS (DynamoDB).</p>
            <ul>
                <li>FaaS - Function as s Service</li>
            </ul>


            <br><br>
            <h2 class="subtitle">CMS / Shop / Assets Management / PIM </h2>
            <p>Content Management System is hosted on EC2 instance and is available through private network. On content change necessary changes are transferred content delivery network,</p>
            <ul>
                <li>BassS - Backend as a Service</li>
            </ul>

            <p>Runs all the time.</p>


            <br><br>
            <h2 class="subtitle">Business Logic</h2>
            <p>Business logic is hosted on EC2.</p>
            <ul>
                <li>BassS - Backend as a Service</li>
            </ul>

            <p>Runs when needed - opening hours...</p>

            <br><br>
            <h2 class="subtitle">Styles and Javascript</h2>
            <p>Content Delivery Network</p>
            <p>Javascript defines browser routes and uses browser storage for user application state.</p>

            <img src="https://www.lucidchart.com/publicSegments/view/c071b708-908e-494d-b471-06cafbfee461/image.png" alt="">

        </div>
    </section>

    <section class="section">
        <h1 class="title">Create a css, js with version number</h1>
        <ul>
            <li>using yarn</li>
            <li>using npm</li>
        </ul>

    </section>

@endsection