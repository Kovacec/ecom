@extends('layouts.app')

@section('content')
<div class="section">
    <div class="container">


        <h1 class="title">Edit: {{ $entity->name }}</h1>


        <form action="/entity/{{ $entity->id }}" method="POST" enctype="multipart/form-data">

            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <input type="hidden" id="entity-id" value="{{ $entity->id }}">



            <div class="columns box">
                <div class="column is-half">
                    <div class="field">
                        <label for="name" class="label">Name:</label>
                        <input type="text"
                               class="input{{ $errors->has('name') ? ' is-danger' : '' }}"
                               id="name"
                               name="name"
                               placeholder="Name"
                               value="{{ $entity->name }}">

                        @if ($errors->has('name'))
                            <p class="help is-danger">
                                {{ $errors->first('name') }}
                            </p>
                        @endif

                    </div>


                    <div class="field">
                        <label for="description" class="label">Description:</label>
                        <textarea name="description"
                                  id="description"
                                  class="textarea"
                                  placeholder="Description"
                                  rows="10">{{ $entity->description }}</textarea>
                    </div>


                    <div class="field">

                        <label for="logo" class="label">Logo</label>


                        @if($entity->logo)
                            <h1>{{ $entity->logo }}</h1>
                            <img src="{{ Storage::disk('s3')->url($entity->logo)  }}" alt="" style="width: 100%">
                        @endif

                        <div class="file">
                            <label for="logo">
                                <input type="file" class="file-input" name="logo" id="logo">
                                <span class="file-cta">
                                <span class="file-icon">
                                    <i class="fa fa-upload"></i>
                                </span>
                                <span class="file-label">
                                  Choose a file…
                                </span>
                            </span>
                            </label>
                        </div>

                    </div>
                </div>

                <div class="column is-half">
                    <div class="field">
                        <label for="tel" class="label">Tel:</label>
                        <p class="control has-icons-left">
                            <input type="text" class="input" id="tel" name="tel" placeholder="Telephone number" value="{{ $entity->tel }}">
                            <span class="icon is-small is-left">
                            <i class="fa fa-phone"></i>
                        </span>
                        </p>

                    </div>


                    <div class="field">
                        <label for="email" class="label">Email:</label>
                        <p class="control has-icons-left">
                            <input type="text" class="input" id="email" name="email" placeholder="Email" value="{{ $entity->email }}">
                            <span class="icon is-small is-left">
                            <i class="fa fa-envelope"></i>
                        </span>
                        </p>
                    </div>

                    <div class="field">
                        <label for="street" class="label">Street:</label>
                        <p class="control has-icons-left">
                            <input type="text" class="input" id="street" name="street" placeholder="Street" value="{{ $entity->street }}">
                            <span class="icon is-small is-left">
                            <i class="fa fa-home"></i>
                        </span>
                        </p>
                    </div>

                    <div class="field">
                        <label for="zip" class="label">ZIP:</label>
                        <p class="control has-icons-left">
                            <input type="text" class="input" id="zip" name="zip" placeholder="Zip" value="{{ $entity->zip }}">
                            <span class="icon is-small is-left">
                            <i class="fa fa-home"></i>
                        </span>
                        </p>
                    </div>

                    <div class="field">
                        <label for="city" class="label">City:</label>
                        <p class="control has-icons-left">
                            <input type="text" class="input" id="city" name="city" placeholder="City" value="{{ $entity->city }}">
                            <span class="icon is-small is-left">
                            <i class="fa fa-home"></i>
                        </span>
                        </p>
                    </div>

                    <div class="field">
                        <label for="website" class="label">Website:</label>
                        <p class="control has-icons-left">
                            <input type="text" class="input" id="website" name="website" placeholder="Website" value="{{ $entity->website }}">
                            <span class="icon is-small is-left">
                            <i class="fa fa-mouse-pointer"></i>
                        </span>
                        </p>
                    </div>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="columns">
    <div class="column is-half is-offset-one-quarter">

        </div>
    </div>

@endsection


@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/entity/edit.js') }}"></script>
@endsection