
<div class="column is-one-quarter">
    <nav class="panel">

        {{--
        <p class="panel-heading">
            Actions
        </p>
        --}}

        <a class="panel-block{{ (route('entities') == Request::url()) ? ' is-active' : '' }}" href="{{ route('entities') }}">
                    <span class="panel-icon">
                      <i class="fa fa-list"></i>
                    </span>
            All
        </a>

        <a class="panel-block{{ (route('entity.create') == Request::url()) ? ' is-active' : '' }}" href="{{ route('entity.create') }}">
                    <span class="panel-icon">
                      <i class="fa fa-plus"></i>
                    </span>
            Create
        </a>

    </nav>
</div>


{{--
<div class="col-md-4">
    <div class="panel panel-default">

        <div class="panel-heading">API KEYs</div>

        <div class="panel-body">

            <div id="entity-api-key">
                @{{ entityApiKey }}
            </div>


            <div class="modal fade" id="apiKeyModal"
                 tabindex="-1" role="dialog"
                 aria-labelledby="apiKeyModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">



                            <div class="modal-header">
                                <button type="button" class="close"
                                        data-dismiss="modal"
                                        aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title"
                                    id="apiKeyModalLabel">Add API KEY</h4>
                            </div>
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="api-type">Select list:</label>
                                    <select class="form-control" id="api-type" required>
                                        <option value="pimcore_webservice">Pimcore Web Service</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <input type="text" name="api-key" id="api-key" class="form-control" required>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button"
                                        class="btn btn-default"
                                        data-dismiss="modal">Close
                                </button>

                              <button type="button"
                                      class="btn btn-primary"
                                      id="set-api-key"
                                      v-on:click="updateApiKey"
                                      data-dismiss="modal">
                                    SET API KEY
                              </button>
                            </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <button
                    type="button"
                    class="btn btn-primary"
                    data-toggle="modal"
                    data-target="#apiKeyModal">
                Add API KEY
            </button>
        </div>

    </div>


    <div class="panel panel-default">
        <div class="panel-heading">Tags</div>
        <div class="panel-body">
        </div>
    </div>


</div>
--}}