@extends('layouts.app')

@section('content')
<div class="section">

    <div class="container">

        <div class="columns">

            @include('entity.partials.sidebar')

            <div class="column">

                <div class="field">
                    <p class="control has-icons-left">
                        <input class="input" type="text" placeholder="Filter" disabled>
                        <span class="icon is-small is-left">
                <i class="fa fa-search"></i>
            </span>
                    </p>
                </div>


                <table class="table is-striped is-fullwidth">
                    <thead>
                    <tr>
                        {{-- <th><abbr title="id">ID</abbr></th> --}}
                        <th>Details</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        {{-- <th><abbr title="id">ID</abbr></th> --}}
                        <th>Details</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($entities as $entity)
                        <tr>
                            {{-- <td>{{ $entity->id }}</td> --}}
                            <td><a href="{{ route('entity', $entity->id) }}" @click="clearItems">{{ $entity->name }}</a></td>
                            <td>{{ $entity->description }}</td>
                            <td style="white-space: nowrap">
                                <a href="/entity/edit/{{ $entity->id }}" class="button is-warning">edit</a>
                                <a href="/entity/{{ $entity->id }}" @click.prevent="" class="button is-danger">delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>


                </table>


            </div>






        </div>
    </div>
</div>
@endsection