@extends('layouts.app')


@section('content')
<div class="section">
    <div class="container">


        <div class="columns">
            @include('entity.partials.sidebar')

            <div class="column">

                <form action="/entity" method="POST" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="box">

                        <h1 class="title">Create entity</h1>

                        <div class="columns">

                            <div class="column is-half">

                                <div class="field">
                                    <label for="name" class="label">Name:</label>
                                    <input type="text"
                                           class="input{{ $errors->has('name') ? ' is-danger' : '' }}"
                                           id="name"
                                           name="name"
                                           value="{{ old('name') }}"
                                           placeholder="Name">

                                    @if ($errors->has('name'))
                                        <p class="help is-danger">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif

                                </div>


                                <div class="field">
                                    <label for="description">Description:</label>
                                    <textarea name="description"
                                              id="description"
                                              class="textarea"
                                              rows="10"
                                              placeholder="Description">{{ old('description') }}</textarea>
                                </div>

                                <div class="field">
                                    <label for="logo" class="label">Logo:</label>
                                    <input type="file"
                                           name="logo"
                                           id="logo"
                                           value="{{ old('logo') }}"
                                           class="file">
                                </div>

                            </div>
                            <div class="column is-half">
                                <div class="field">
                                    <label for="tel" class="label">Tel:</label>
                                    <p class="control has-icons-left">
                                        <input type="text"
                                               class="input"
                                               id="tel"
                                               name="tel"
                                               value="{{ old('tel') }}"
                                               placeholder="Telephone number">
                                        <span class="icon is-small is-left">
                                <i class="fa fa-phone"></i>
                            </span>
                                    </p>
                                </div>


                                <div class="field">
                                    <label for="email" class="label">Email:</label>
                                    <p class="control has-icons-left">
                                        <input type="text"
                                               class="input"
                                               id="email"
                                               name="email"
                                               value="{{ old('name') }}"
                                               placeholder="Email">
                                        <span class="icon is-small is-left">
                                <i class="fa fa-envelope"></i>
                            </span>
                                    </p>
                                </div>

                                <div class="field">
                                    <label for="street" class="label">Street:</label>
                                    <p class="control has-icons-left">
                                        <input type="text"
                                               class="input"
                                               id="street"
                                               name="street"
                                               value="{{ old('street') }}"
                                               placeholder="Street">
                                        <span class="icon is-small is-left">
                                <i class="fa fa-home"></i>
                            </span>
                                    </p>
                                </div>

                                <div class="field">
                                    <label for="zip" class="label">ZIP:</label>
                                    <p class="control has-icons-left">
                                        <input type="text"
                                               class="input"
                                               id="zip"
                                               name="zip"
                                               value="{{ old('zip') }}"
                                               placeholder="Zip">
                                        <span class="icon is-small is-left">
                                <i class="fa fa-home"></i>
                            </span>
                                    </p>
                                </div>

                                <div class="field">
                                    <label for="city" class="label">City:</label>
                                    <p class="control has-icons-left">
                                        <input type="text"
                                               class="input"
                                               id="city"
                                               name="city"
                                               value="{{ old('city') }}"
                                               placeholder="City">
                                        <span class="icon is-small is-left">
                                <i class="fa fa-home"></i>
                            </span>
                                    </p>
                                </div>

                                <div class="field">
                                    <label for="website" class="label">Website:</label>
                                    <p class="control has-icons-left">
                                        <input type="text"
                                               class="input"
                                               id="website"
                                               name="website"
                                               value="{{ old('website') }}"
                                               placeholder="Website">
                                        <span class="icon is-small is-left">
                                <i class="fa fa-mouse-pointer"></i>
                            </span>
                                    </p>
                                </div>

                                <div class="field">
                                    <div class="control">
                                        <button type="submit" class="button is-primary">Create</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection