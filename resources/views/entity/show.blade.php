@extends('layouts.app')


@section('content')

<div class="section">
    <div class="container">
        <div class="columns">
            <div class="column">
                <h1 class="title">
                    {{ $entity->name }}
                </h1>
                <p>
                    {{ $entity->description }}
                </p>
            </div>
            <div class="column is-one-quarter">
                <nav class="panel">
                    <p class="panel-heading">
                        Actions
                    </p>

                    <a class="panel-block"
                       href="{{ route('item.create', ['entity' => $entity->id]) }}"
                       @click="removeItems">
                    <span class="panel-icon">
                      <i class="fa fa-plus"></i>
                    </span>
                        Add Item
                    </a>

                </nav>
            </div>
        </div>


    </div>
</div>


<section class="section">
    <div class="container">
        <items-b2c></items-b2c>
    </div>
</section>


@endsection


@section('scripts')
    <script>
        app.setItems('entity_id={{ $entity->id }}')
    </script>
@endsection


