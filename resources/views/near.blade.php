@extends('layouts.test')

@section('content')
    <div class="hero" style="height: 100vh">
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <div class="tile">
                    <div class="tile">
                        <article class="tile is-child">
                            <div id="locationApp">
                                <div class="notification" id="searchBar">
                                    <div class="control has-icons-left has-icons-right">
                                        <input class="input" type="text" placeholder="Search">
                                        <span class="icon is-small is-left">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                                <a href="#" class="list-item" v-for="item in items" class="list-item" :rel="this.id" @click="details(item)">
                                    <span class="name">@{{ item.name }} lat: @{{ item.latitude  }} lng: @{{ item.longitude }}</span><br>
                                    <span class="address">@{{ item.street }} @{{ item.zip  }} @{{ item.city  }}</span>
                                </a>
                            </div>
                        </article>
                    </div>
                    <div class="tile">
                        <div class="tile is-child" id="map">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgfF2N2CYEbRfUYf4-YuIxx8g00ehWBWk"></script>
    <script>
        // initialize google maps
        // var icon = '/img/location.png';
        var initialCenter = {
            lat: 50.237977,
            lng: 11.321541
        };

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: initialCenter,
            styles: [{
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#e0efef"}]
            }, {
                "featureType": "poi",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"hue": "#1900ff"}, {"color": "#c0e8e8"}]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{"lightness": 100}, {"visibility": "simplified"}]
            }, {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{"visibility": "on"}, {"lightness": 700}]
            }, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#7dcdcd"}]}]
        });


        // initilize app
        var data = {
            selectedItem: 'test',
            items: []
        };

        Vue.component('list-item', {
            props: ['id', 'name', 'description', 'street', 'city', 'zip', 'latitude', 'longitude'],
            template: "#list-item",
            data: function () {
                return {
                    show: true
                };
            },
            methods: {
                details: function (e) {

                    console.log(e)
                    // console.log(data.items)

                    // map.panTo()
                    // get item detail and display it
                    /*

                    axios.get('/en/item/short/' + this.id)
                        .then(function (response) {
                            // console.log(response)
                            document.getElementById("details").innerHTML = response.data;
                        });

                    */

                }
            }

        })

        list = new Vue({
            el: '#locationApp',
            data: data,
            methods: {
                details: function (item) {
                    map.setCenter(new google.maps.LatLng(item.latitude, item.longitude))
                }
            }
        });


        // https://developers.google.com/maps/documentation/javascript/events
        map.addListener('idle', function () {

            //console.log(map.getCameraPosition().zoom);

            axios.post('/bounds', map.getBounds())
                .then(function (response) {

                    clearMarkers()

                    if (response.data.items.length > 0) {

                        data.items = []

                        for (var i = 0; i < response.data.items.length; i++) {

                            response.data.items[i] = addMarker(response.data.items[i])

                            data.items.push(
                                response.data.items[i]
                            )

                        }

                        setMapOnAll(map)
                    }


                })
                .catch(function (error) {
                    console.log(error)
                })

        });

        // add marker to object item
        function addMarker(item) {

            var position = {
                lat: parseFloat(item.latitude),
                lng: parseFloat(item.longitude)
            };

            console.log(position, 'position')
            console.log(item, 'item')




            let icon = item.icon;

            var markerSettins = {
                position: position,
                    // animation: google.maps.Animation.DROP,
                    icon: icon,
                    map: null
            }


            var marker = new google.maps.Marker(markerSettins);

            item['marker'] = marker;

            return item;
        }

        // Sets the map on all markers in the array.
        function setMapOnAll(map) {

            for (var i = 0; i < data.items.length; i++) {
                // if (data.items[i]['marker'] != 'undefined') {
                data.items[i]['marker'].setMap(map);
                // }
            }

        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setMapOnAll(null);
        }


        function setHeight() {
            document.getElementById('')
        }


    </script>



    <style>

        .tile, .tile.is-parent {
            margin: 0 !important;
            padding: 0 !important;
        }

        .list-item {
            display: block;
            padding: 10px;
            font-size: small;
            border-bottom: 1px solid whitesmoke;
        }

        .list-item a {

        }

        .name {
            font-family: Serif;
            font-size: large;
        }

        #locationApp {
            height: 100vh;
            overflow-y: scroll;
        }

        #searchBar {
            position: fixed;
            margin-left: 25%;
        }


    </style>

@endsection