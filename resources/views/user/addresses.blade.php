@extends('layouts.app')

@section('content')
    <div class="section">
        <div class="container">
            <div class="columns">

                @include('user.partials.sidebar')

                <div class="column">
                    <div class="notification">

                        <h1 class="title">Your addresses</h1>
                        <addresses></addresses>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
