@extends('layouts.app')

@section('content')
    <div class="section">
        <div class="container">
            <div class="columns">

                @include('user.partials.sidebar')

                <div class="column">
                    <div class="notification">

                        <h1 class="title">Your orders 1</h1>
                        <div id="orders">
                            @foreach($orders as $order)
                                <div class="card">
                                    <header class="card-header">
                                        <p class="card-header-title">
                                            Order: #{{ $order->id }}
                                        </p>
                                    </header>
                                    <div class="card-content">
                                        <div class="content">

                                            <div class="columns">
                                                <div class="column">
                                                    <p>Order at: {{ $order->created_at }}</p>
                                                    <p>Arriving: SOME DATA</p>
                                                    <p>STATUS</p>
                                                </div>
                                            </div>

                                            {{--
                                            <br>
                                            <div>
                                                <button class="button is-warning">TEST</button>
                                                <button class="button is-warning">TEST</button>
                                                <button class="button is-warning">TEST</button>
                                            </div>
                                            --}}

                                            <br>
                                            <div>
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Price</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($order->positions as $position)
                                                        <tr>
                                                            <td>{{ $position->item_name }}</td>
                                                            <td>{{ $position->item_price }} EUR</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <td>Total:</td>
                                                        <td>{{ $order->total }} EUR</td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

