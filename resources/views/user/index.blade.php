@extends('layouts.app')

@section('content')
    <div class="section">
        <div class="container">
            <div class="columns">

                @include('user.partials.sidebar')

                <div class="column">
                    <div class="notification">

                        <h1 class="title">Profidddle data</h1>
                        <h1 class="title">Profidddle data</h1>

                        <p>
                            {{ Auth::user()->name }} <br>
                            {{ Auth::user()->email }} <br>
                        </p>
                        <a href="#" @click.prevent="itemShowEditProfile">Change</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection