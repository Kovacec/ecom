<div class="col-md-4">
    <div class="panel panel-default">

        <div class="panel-heading">Categories</div>

        <div class="panel-body">
            <ul>
            @foreach($categories as $category)
                <li>
                    <a href="/items/category/{{ $category }}">{{ $category }}</a>
                </li>
            @endforeach
            </ul>
        </div>

    </div>


    <div class="panel panel-default">

        <div class="panel-heading">Tags</div>

        <div class="panel-body">
            <ul>
                @foreach($tags as $tag)
                    <li>
                        <a href="/items/tags/{{ $tag }}">{{ $tag }}</a>
                    </li>
                @endforeach
            </ul>
        </div>

    </div>
</div>
