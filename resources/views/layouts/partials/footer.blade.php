<footer class="footer">
    <div class="container">
        <div class="content has-text-centered">
            <p>
                <strong>{{ config('app.name', 'set app name') }}</strong> by <a href="#">https://</a>. &copy; {{ date('Y') }}
            </p>
            <p>{{-- <a href="/newsletter">Newsletter</a> | --}}<a href="/contact">contact.us</a> | <a href="/legal">Terms and conditions</a></p>
            <p>
                <a class="icon" href="#">
                    <i class="fa fa-facebook"></i>
                </a>
                <a class="icon" href="#">
                    <i class="fa fa-twitter"></i>
                </a>
                <a class="icon" href="#">
                    <i class="fa fa-instagram"></i>
                </a>
            </p>
            {{--
            <p>
                <a href="https://gitlab.com/slavic-group/ecom/commits/master"><img alt="pipeline status" src="https://gitlab.com/slavic-group/ecom/badges/master/pipeline.svg" /></a>
                <a href="https://gitlab.com/slavic-group/ecom/commits/master"><img alt="coverage report" src="https://gitlab.com/slavic-group/ecom/badges/master/coverage.svg" /></a>
            </p>
            --}}
        </div>
    </div>
</footer>