<nav class="navbar">
    <div class="navbar-brand">
        <a class="navbar-item" href="/">
            {{ config('app.name', 'set app name') }}
            <!--
            style="position: fixed !important; top: 0; left: 0; right: 0; z-index: 1000"

            <img src="http://bulma.io/images/bulma-logo.png" alt="Bulma: a modern CSS framework based on Flexbox"
                 width="112" height="28">
                 -->
        </a>

        <a class="navbar-item is-hidden-desktop" href="#" target="_blank">
          <span class="icon">
            <i class="fa fa-user"></i>
          </span>
        </a>

        <a class="navbar-item is-hidden-desktop" href="#" target="_blank">
          <span class="icon">
            <i class="fa fa-user-plus"></i>
          </span>
        </a>

        <div class="navbar-burger burger" data-target="navMenubd-example">
            <span></span>
            <span></span>
            <span></span>
        </div>

    </div>

    <div class="navbar-menu">

        <div class="navbar-start">

            @if(Auth::check() && (Auth::user()->hasRole('owner') || Auth::user()->hasRole("customer")))

                {{--
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link is-active" href="/items">
                        Items
                    </a>

                    <div class="navbar-dropdown ">
                        <a class="navbar-item " href="/item/create">
                            Create
                        </a>


                        <a class="navbar-item " href="http://bulma.io/documentation/modifiers/syntax/">
                            Properties
                        </a>

                        <hr class="navbar-divider">
                        <div class="navbar-item">
                            <div>
                                <small>
                                    V: 0.0.1
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                --}}

                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link is-active" href="/entities">
                        Entities
                    </a>

                    <div class="navbar-dropdown ">
                        <a class="navbar-item " href="/entity/create">
                            Create
                        </a>

                        {{--
                        <a class="navbar-item " href="http://bulma.io/documentation/modifiers/syntax/">
                            Properties
                        </a>
                        --}}


                        <hr class="navbar-divider">
                        <div class="navbar-item">
                            <div>
                                <small>
                                    V: 0.0.1
                                </small>
                            </div>
                        </div>
                    </div>
                </div>





                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link is-active" href="/orders">
                        Orders
                    </a>

                    <div class="navbar-dropdown ">

                        {{--
                        <a class="navbar-item " href="http://bulma.io/documentation/modifiers/syntax/">
                            Properties
                        </a>
                        --}}

                        <hr class="navbar-divider">
                        <div class="navbar-item">
                            <div>
                                <small>
                                    V: 0.0.1
                                </small>
                            </div>
                        </div>
                    </div>
                </div>

            @endif

        </div>


        <div class="navbar-end">

            <div class="navbar-item">

                <transition name="fade">
                    <p class="control" v-show="showCheckout">
                        <a class="button is-primary" href="/checkout">
                            <span class="icon">
                              <i class="fa fa-shopping-basket"></i>
                            </span>
                            <span>
                                <span v-show="weight > 0">@{{ weight }} kg </span> Checkout @{{ animatedTotal }} EUR
                            </span>
                        </a>
                    </p>
                </transition>

            </div>

            @if (Auth::guest())

                <div class="navbar-item">
                    <a class="" href="{{ route('login') }}">
                        Login
                    </a>
                </div>

                <div class="navbar-item">
                    <a class="" href="{{ route('register') }}">
                        Register
                    </a>
                </div>

            @else


                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="/user">
                        {{ Auth::user()->name }}
                    </a>

                    <div class="navbar-dropdown is-right">


                        {{--
                        <a class="navbar-item" href="{{ route('user.payment') }}">
                            Payment
                        </a>
                        --}}

                        <a class="navbar-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                    </div>
                </div>

            @endif
        </div>

    </div>
</nav>
