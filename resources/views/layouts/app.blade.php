<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'set app name') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <script src="https://cdn.jsdelivr.net/npm/tween.js@16.3.4"></script>

</head>
<body>

<div id="app" style="position: relative">
    <flash message="Spletna stran v izdelavi!"></flash>
    @include('layouts.partials.nav')
    <div id="content">
        @yield('content')
    </div>
    @include('layouts.partials.footer')
</div>


<script src="{{ asset('js/app.js') }}"></script>
@yield('scripts')

</body>
</html>

