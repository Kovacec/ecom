@extends ('layouts.app')


@section ('content')

    <section class="hero is-fullheight" style=" background-image: url('https://wallpapercave.com/wp/wXDMMuq.jpg') ;
        background-position: center center;
        background-repeat:  no-repeat;
        background-attachment: fixed;
        background-size:  cover;
        background-color: #999;">
        <div class="hero-body">
            <div class="container">

                <div class="columns">

                    <div class="column is-half notification" v-show="showNewsletterNotification">
                        <a href="#" class="button is-primary" @click="toggleNewsletterRegistration">Register</a>
                    </div>

                    <div class="column is-half animated fadeInLeft notification" v-show="showNewsletterForm">

                        <h1 class="title text-info bounce">
                            Newsletter registration
                        </h1>
                        <h2 class="subtitle">

                        </h2>

                        <form action="/newsletter" action="post">
                            <div class="field">
                                <label class="label">E-mail</label>
                                <div class="control">
                                    <input class="input"
                                           type="text"
                                           name="email"
                                           id="email"
                                           placeholder="E-mail">
                                </div>

                                <p class="help">This is a help text</p>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <button type="submit" class="button is-primary">Submit</button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>


            </div>
        </div>
    </section>

@endsection