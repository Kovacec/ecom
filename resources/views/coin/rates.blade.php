@extends('layouts.app')

@section('content')
<div class="container">

    <table class="table is-striped">
        <thead>
        <tr>
            <th>COIN</th>
            <th>Name</th>
            <th>FIAT</th>
            <th>BTC</th>
            <th>LastUpdate</th>
            <th>Fee</th>
            {{-- <th>Status</th> --}}
            <th>Confirms</th>
            <th>CanConvert</th>
            {{-- <th>Capabilities</th> --}}
        </tr>
        </thead>

        <tbody>
        @foreach($rates as $coin => $rate)
            <tr>
                <td>{{ $coin }}</td>
                <td nowrap>{{ $rate['name'] }}</td>
                <td>{{ $rate['is_fiat'] }}</td>
                <td>{{ $rate['rate_btc'] }}</td>
                <td>{{ $rate['last_update'] }}</td>
                <td>{{ $rate['tx_fee'] }}</td>
                <td>{{ $rate['status'] }}</td>
                <td>{{ $rate['confirms'] }}</td>
                <td>{{ $rate['can_convert'] }}</td>
                {{-- <td>{{ implode(',', $rate['capabilities']) }}</td> --}}
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@endsection