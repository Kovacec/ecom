@extends('layouts.app')

@section('content')
    <div class="section">
        <div class="container">
            <div class="main-carousel" data-flickity='{ "contain": true }'>
                <div class="carousel-cell" style="width: 100%; margin-right: 10px; height: 400px;">
                    <section class="hero is-primary is-medium" style="height: 400px; background: url('/img/warren-wong-320130.jpg'); background-position: center center;  background-repeat:  no-repeat;background-attachment: fixed;      background-size:  cover;">
                        <div class="hero-body">
                            <div class="container" style="text-align: center">
                                <h1 class="title" style="color: black">
                                    Društvo za promocijo ekoloških izdelkov
                                </h1>
                                <h2 class="subtitle" style="color: black">
                                    digitalizacija in promocija ekoloških izdelkov
                                </h2>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="carousel-cell" style="width: 100%; margin-right: 10px">
                    <div style="width: 100%; height: 400px; background: url('/img/warren-wong-320130.jpg'); background-position: center center;  background-repeat:  no-repeat;background-attachment: fixed;      background-size:  cover;"></div>
                </div>
            </div>
        </div>
    </div>su

    <div class="section">
        <div class="container">

            {{-- slide --}}

            {{--
            <div class="tile is-ancestor">

                <div class="tile is-parent animated fadeInLeft">
                    <article class="tile is-child notification">
                        <p class="title">Shops</p>
                        <p class="subtitle">
                            E-commerce systems
                        </p>
                        <p>
                            <a href="/b2c" class="button is-primary">Available products</a>
                        </p>
                    </article>
                </div>


                <div class="tile is-parent animated fadeInRight">
                    <article class="tile is-child notification is-primary">
                        <p class="title">Crete own</p>
                        <p class="subtitle">E-commerce system as service</p>
                        <p>
                            <a href="/kontakt" class="button is-warning" @click.prevent="showContactForm = true">contact.us</a>
                        </p>
                    </article>
                </div>


            </div>
            --}}


            {{--
            <div class="container has-text-centered">
                <div class="column is-6 is-offset-3">
                    <h1 class="title">
                        Coming Soon
                    </h1>
                    <h2 class="subtitle">
                       ecom as SasS build with laravel an vuejs
                    </h2>
                    <div class="box">

                        <div class="field is-grouped">
                            <p class="control is-expanded">
                                <input class="input" type="text" placeholder="Enter your email">
                            </p>
                            <p class="control">
                                <a class="button is-info">
                                    Notify Me
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            --}}


            {{--
            <div class="tile is-ancestor">
                <div class="tile is-vertical is-8">
                    <div class="tile">


                        <div class="tile is-parent">
                            <article class="tile is-child box">
                                <p class="title">Dostava</p>
                                <p class="subtitle"><a href="/delivery">Izberite lokalne produkte</a></p>
                                <figure class="image is-4by3">
                                    <img src="http://bulma.io/images/placeholders/640x480.png">
                                </figure>
                            </article>
                        </div>



                        <div class="tile is-parent is-vertical">
                            <article class="tile is-child notification">
                                <p class="subtitle"><a href="/b2c">Seznam izdelkov</a></p>
                                <p class="subtitle"><a href="/categories">Brskaj po kategorijah</a></p>
                                <p class="subtitle"><a href="/box">Ponudba zabojckov</a></p>
                            </article>
                        </div>
                    </div>
                    <div class="tile is-parent">
                        <article class="tile is-child notification">
                            <p class="title">Podpora</p>
                            <p>
                                <a href="/kontakt" class="button is-primary" @click.prevent="showContactForm = true">contact.us</a>
                            </p>
                            <br>
                            <p>
                                Klepetalnica: gitter
                            </p>
                            <br>
                            <p>
                                Tel: XXXXXX
                            </p>
                        </article>
                    </div>
                </div>
                <div class="tile is-parent">
                    <article class="tile is-child notification is-success">
                        <div class="content">
                            <p class="title">Semenska banka</p>
                            <p class="subtitle">Spletna izmenjava semena</p>
                            <div class="content">
                                <p>Tudi sami pridelujete svojo hrano?</p>
                                <p>Raznovrstnost umira. Vse vec je ponudbe a vedno majn raznolike. Cilj se ustvariti spletno bazo vseh vrst ki se se pri nas se uporabljajo in dati moznost uporabnikom da si izmenjujejo semena.</p>
                                <p></p>
                                <p class="subtitle">Anketa</a>
                                <p>Tudi sami pridelujete hrano - imate vrt</p>
                                <p>Ja / Ne</p>
                                <p>Kupujete lokalna semena</p>
                                <p></p>
                                <p>Bi uporabljali spletno mesto za izmenjavo semen</p>
                                <p>Ja / Ne</p>
                                <p>Komentar</p>
                                <p></p>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
            --}}

        </div>
    </div>


    @include('message.modal')


@endsection


@section('scripts')
    <script>
    </script>
@endsection