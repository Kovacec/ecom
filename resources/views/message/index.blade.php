@extends('layouts.app')

@section('content')
    <div class="section">
        <div class="container">
            <div class="columns">

                <div class="column is-half is-offset-one-quarter box">
                    <h1 class="title">Contact us</h1>

                    <form class="form-horizontal"
                          method="POST"
                          action="{{ route('message.send') }}"
                          @submit.prevent="onContactSubmit"
                    @keydown="contactForm.errors.clear($event.target.name)">
                    @include('message.partials.inputs')
                    <button :class="{'button is-success': true, 'is-loading': contactForm.submitting }"
                            :disabled="contactForm.errors.any()">{{ trans('message.send') }}</button>

                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

    </script>
@endsection