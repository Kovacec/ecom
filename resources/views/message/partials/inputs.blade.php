@if(!empty($item))
    <div class="field is-hidden">

        <label for="subject" class="label">{{-- trans('contact.subject') --}}Zadeva</label>
        <input id="subject"
               type="text"
               class="{'input': true, 'has-danger': contactForm.errors.get('subject')}"
               name="subject"
               v-model="contactForm.subject"
               value="item:{{ $item->id }}:{{ $item->name }}" autofocus
        >
        <span class="help is-danger" v-text="contactForm.errors.get('subject')"></span>

    </div>
@else
    <div class="field">
        <label for="subject" class="label">{{-- trans('message.subject') --}}zadeva</label>
        <input id="subject"
               type="text"
               class="input"
               name="subject"
               v-model="contactForm.subject"
               placeholder="{{-- trans('message.subject') --}}zadeva"
               value="" autofocus
        >
        <span class="help is-danger" v-text="contactForm.errors.get('subject')"></span>
    </div>
@endif

<div class="field">
    <label for="from" class="label">{{-- trans('message.from') --}}od</label>
    <input id="from"
           type="text"
           class="input"
           name="from"
           v-model="contactForm.from"
           placeholder="{{-- trans('message.from') --}}od"
           value="" autofocus
    >

    <span class="help is-danger" v-text="contactForm.errors.get('from')"></span>

</div>


<div class="field">
    <label for="message" class="label">{{-- trans('message.body') --}}sporočilo</label>

    <textarea id="body"
              class="textarea"
              name="body"
              v-model="contactForm.message"
              placeholder="{{-- trans('message.body') --}}sporočilo"
    ></textarea>

    <span class="help is-danger" v-text="contactForm.errors.get('body')"></span>

</div>