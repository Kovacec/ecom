{{-- Contact form --}}
<form class="form-horizontal"
      method="POST"
      action="{{ route('message.send') }}"
      @submit.prevent="onContactSubmit"
      @keydown="contactForm.errors.clear($event.target.name)">

    {{ csrf_field() }}

    <modal v-show="showContactForm" @close="showContactForm = false">
        <template slot="header">{{ trans('contact.us') }}</template>

        <template slot="content">
            @include('message.partials.inputs')
        </template>

        <template slot="footer">
            <button :class="{'button is-success': true, 'is-loading': contactForm.submitting }"
                    :disabled="contactForm.errors.any()">{{ trans('message.send') }}</button>
            <button class="button" @click="showContactForm = false">Cancel</button>
        </template>

    </modal>

</form>