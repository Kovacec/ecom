@extends('layouts.app')


@section('content')


    <div class="section">
        <h1 class="title">
            Ponudite svojo ponudbo na spletu (ecom)
        </h1>

        <p>Na enostaven nacin lahko s par kliki svojo ponubo izdelkov ponujate na spletu.</p>
        <p>Opcijsko vam pomagamo predstavitnevno stran vase kmetije ali obrti.</p>
    </div>


    <div class="section">
        <h2 class="subtitle">
            Minimalni stroski (SaaS)
        </h2>

        <p>
            Z prodajo na spletni applikaciji placate samo stroske prodaje (transakcij) in vzdrzevanje programske opreme. <br>
        </p>
    </div>

    <div class="section">

        <div class="columns">
            <div class="column">
                <h3 class="title">Paketi</h3>

                <table class="table is-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Osnovni</th>
                        <th>Dodatni</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Mesecna cena</td>
                        <td>0</td>
                        <th>100</th>
                    </tr>

                    <tr>
                        <td>Stroski nakupa s kartico</td>
                        <td>1.9% + 30c</td>
                        <td>1.9% + 30c</td>
                    </tr>

                    <tr>
                        <td>Stroski prenosa na vas racun</td>
                        <td>TODO</td>
                        <td>TODO</td>
                    </tr>

                    <tr>
                        <td>Stroski programske opreme</td>
                        <td>5 %</td>
                        <td>2 %</td>
                    </tr>


                    <tr>
                        <td>Staff accounts</td>
                        <td>2</td>
                        <td>10 <a href="">vec</a></td>
                    </tr>

                    <tr>
                        <td>Stevilo produktov</td>
                        <td>neomejeno</td>
                        <td>neomejeno</td>
                    </tr>


                    <tr>
                        <td>Tiskanje printing labels</td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                    </tr>

                    <tr>
                        <td>Podpora preko emaila</td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                    </tr>

                    <tr>
                        <td>Predstavitvena stran</td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                    </tr>

                    <tr>
                        <td>SSL certifikat</td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                    </tr>

                    <tr>
                        <td>Podpora preko telefona</td>
                        <td></td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                    </tr>

                    <tr>
                        <td>Kode za popust</td>
                        <td></td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                    </tr>

                    <tr>
                        <td>Analiza prevar</td>
                        <td></td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                    </tr>


                    <tr>
                        <td>Pomoc pri aktivnostih</td>
                        <td></td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                    </tr>

                    <tr>
                        <td>Social media support</td>
                        <td></td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <div class="column">

                <h3 class="title">Brezplacna registracija</h3>
                <a href="/register/saas" class="button is-warning">Postani partner</a>

            </div>

        </div>

    </div>






@endsection
