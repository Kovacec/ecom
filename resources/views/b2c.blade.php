@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="container">

            <div class="field">
                <p class="control has-icons-left">
                    <input class="input"
                           type="text"
                           placeholder="Filter"
                           v-model="itemsFilter"
                    @keyUp="filterItems">

                    <span class="icon is-small is-left">
                    <i class="fa fa-search"></i>

                </span>
                </p>

            </div>

        </div>
    </section>


    <section class="section">
        <div class="container">
            <items-b2c></items-b2c>
        </div>
    </section>


@endsection

@section('scripts')

    <script>
        console.log(this.app.setItems())


    </script>

@endsection