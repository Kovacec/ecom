@extends('layouts.app')

@section('content')



    <section class="hero is-info is-large">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-half is-offset-one-quarter box">
                        <h1 class="title">Login 1</h1>
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">

                            {{ csrf_field() }}

                            <input type="hidden" name="redirectTo" value="{{ $redirectTo }}" />

                            <div class="field">
                                <label class="label" for="email">E-Mail Address</label>
                                <input id="email"
                                       type="email"
                                       class="input{{ $errors->has('email') ? ' is-danger' : '' }}"
                                       name="email"
                                       value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <p class="help is-danger">
                                        {{ $errors->first('email') }}
                                    </p>
                                @endif

                            </div>

                            <div class="field{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="label">Password</label>
                                <input id="password" type="password" class="input" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                @endif

                            </div>

                            <div class="field">e
                                <div class="control">
                                    <label class="checkbox">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        Remember Me
                                    </label>
                                </div>
                            </div>

                            <div class="field is-grouped">
                                <div class="control">
                                    <button type="submit" class="button is-primary">
                                        Login
                                    </button>
                                </div>

                                <div class="control">
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Forgot Your Password?
                                    </a>
                                </div>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
