<div class="field{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="label">Name</label>
    <input id="name"
           type="text"
           class="input"
           name="name"
           v-model="registrationForm.name"
           value="{{ old('name') }}" required autofocus>

    @if ($errors->has('name'))
        <p class="alert is-dager">
            {{ $errors->first('name') }}
        </p>
    @endif

    <span class="help is-danger" v-text="registrationForm.errors.get('name')"></span>

</div>

<div class="field{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="label">E-Mail Address</label>

    <input id="email"
           type="email"
           class="input"
           name="email"
           v-model="registrationForm.email"
           value="{{ old('email') }}" required>

    @if ($errors->has('email'))
        <p class="alert is-danger">
            {{ $errors->first('email') }}
        </p>
    @endif

    <span class="help is-danger" v-text="registrationForm.errors.get('email')"></span>

</div>

<div class="field{{ $errors->has('password') ? ' has-error' : '' }}">
    <label for="password" class="label">Password</label>
    <input id="password"
           type="password"
           class="input"
           v-model="registrationForm.password"
           name="password" required>

    @if ($errors->has('password'))
        <p class="alert is-danger">
            {{ $errors->first('password') }}
        </p>
    @endif

    <span class="help is-danger" v-text="registrationForm.errors.get('password')"></span>

</div>

<div class="field">
    <label for="password-confirm" class="label">Confirm Password</label>
    <input id="password-confirm"
           type="password"
           class="input"
           v-model="registrationForm.password_confirmation"
           name="password_confirmation" required>
</div>