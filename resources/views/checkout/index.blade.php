@extends('layouts.app')


@section('content')
    <div class="section" v-show="this.$store.getters.cart.length == 0">
        <div class="container">
            <div class="notification is-warning">
                Cart is empty.
            </div>

            <div class="has-text-centered">
                <a href="/b2c">Continue shopping</a>
            </div>
        </div>
    </div>

    <div class="container" v-show="this.$store.getters.cart.length">
        <div class="section">

            <checkout-steps step="shopping-cart"
                            next="/checkout/user-details"
                            next-text="Checkout">
            </checkout-steps>

        </div>
        <div class="section">
            <checkout-shopping-cart step="shopping-cart" can-remove="true"></checkout-shopping-cart>
        </div>
    </div>
@endsection