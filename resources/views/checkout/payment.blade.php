@extends('layouts.app')

@section('content')

    <div class="section">
        <checkout-steps step="payment"
                        prev="/checkout/user-details"
                        prev-text="User Details"
                        type="{{ Auth::check() ? 'standard' : 'guest' }}">
        </checkout-steps>
    </div>

    <div class="columns">
        <div class="column is-half is-offset-one-quarter">
            <form id="checkout" method="post" action="/checkout/charge">

                {{ csrf_field() }}

                <div id="payment-form"></div>

                <button type="submit" class="button is-success" :value="total">
                    Pay @{{ total }} €
                </button>

                {{-- @click="sendCart" --}}

            </form>
        </div>
    </div>


    <div class="section">
        <div class="container">
            <checkout-shopping-cart step="payment"></checkout-shopping-cart>
        </div>
    </div>



@endsection


@section('scripts')
    <script src="https://js.braintreegateway.com/js/braintree-2.32.1.min.js"></script>
    <script>

        var clientToken = "{{ $clientToken }}";

        braintree.setup(clientToken, "dropin", {
            container: "payment-form"
        });

    </script>
@endsection