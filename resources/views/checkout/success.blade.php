@extends('layouts.app')

@section('content')

    <checkout-steps step="checkout-completed"></checkout-steps>

    <div class="columns">
        <div class="column is-half is-offset-one-quarter">
            <h1 class="title">Thank you</h1>
            <div class="has-text-centered">
                <a href="">Orders</a>
            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        this.store.commit('clearCart')
    </script>
@endsection