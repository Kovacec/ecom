@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="section">
            <checkout-steps step="user-details"
                            next="/checkout/payment"
                            next-text="Payment"
                            prev="/checkout"
                            prev-text="Shopping Cart"
                            type="{{ Auth::check() ? 'standard' : 'guest' }}"
                            :registration-form="registrationForm"
                            :address-form="addressForm"
            >
            </checkout-steps>


        </div>

        {{--
        <div class="section">
            <checkout-shopping-cart step="shopping-cart"></checkout-shopping-cart>
        </div>
        --}}

        <div class="section">
            <div class="columns">

                <div class="column is-half is-offset-one-quarter box">

                    @if(!Auth::check())
                        <h2 class="title">Already have an account?</h2>
                        <a href="/login?redirect=/checkout/user-details" class="button is-info">Login</a>
                        <hr>
                        <h2 class="title">
                            <small><i>or</i></small>
                            create an account
                        </h2>

                        @include('auth.partials.register-fields')

                    @endif

                    @if(Auth::user())


                        <p>
                            <small><i>{{ Auth::user()->email }}</i></small>
                        </p>
                        <h1 class="title">{{ Auth::user()->name }}</h1>

                        @include('themes::eco.address.partials.fields')
                        {{--
                        TODO: not in MVP
                        <div class="section text-is-center">
                            <addresses></addresses>
                        </div>
                        --}}
                    @endif
                </div>
            </div>
        </div>
    </div>

    {{--
    TODO: not in MVP
    <modal v-show="showContactForm" @close="showContactForm = false">
        <template slot="header">{{ trans('checkout.address.validation') }}</template>

        <template slot="content">
        </template>

        <template slot="footer">
            <button :class="{'button is-success': true }">{{ trans('address.accept') }}</button>
            <button class="button" @click="showContactForm = false">Cancel</button>
        </template>

    </modal>
    --}}

@endsection


@section('scripts')
    @if(Auth::check())
        <script>
            // set / refresh form data
            app.addressForm.street = '{{ Auth()->user()->street }}'
            app.addressForm.zip = '{{ Auth()->user()->zip }}'
            app.addressForm.city = '{{ Auth()->user()->city }}'
            // not in MVP app.setAddresses()

            // commit current order

        </script>
    @endif
@endsection