@extends('layouts.app')

@section('content')


    <div class="section">
        <div class="container">
            <div class="main-carousel" data-flickity='{ "cellAlign": "left", "contain": true }'>
                <div class="carousel-cell">

                </div>
                <div class="carousel-cell">

                </div>
                <div class="carousel-cell">

                </div>
            </div>
        </div>
    </div>



    <div class="section">
        <div class="container">
            <div class="tile is-ancestor">



                @foreach($columns as $column)
                <div class="tile is-vertical is-one-third">

                        @foreach($column as $category)
                        <div class="tile">
                            <a href="#" rel="category={{ $category->id }}" @click.prevent="queryItems">{{ $category->name }}</a>
                        </div>
                        @endforeach

                </div>
                @endforeach


            </div>
        </div>
    </div>


    <div class="columns">

    </div>

@endsection

