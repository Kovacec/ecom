import axios from 'axios';
import Vue from 'vue'
import {store} from './store'
import Form from './core/Form'
import scrollToY from './core/scrollToY'
import VueAgile from 'vue-agile' // slider
Vue.use(VueAgile)

let token = document.head.querySelector('meta[name="csrf-token"]');
// console.log(token, 'token')

window.axios = axios
window._token = token.content

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    console.log(token.content, 'token')

} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}


// -----------------------------------------------------------
// global events
// -----------------------------------------------------------
window.events = new Vue()

window.flash = function (data) {
    window.events.$emit('flash', data)
}

window.addToCart = function (item) {
    window.events.$emit('addToCart', item)
}

// -----------------------------------------------------------
// vue components
// -----------------------------------------------------------
Vue.component('flash', require('./components/Flash.vue').default);
Vue.component('modal', require('./components/Modal.vue').default);
Vue.component('cart', require('./components/Cart.vue').default);
Vue.component('checkout-steps', require('./components/CheckoutSteps.vue').default);
Vue.component('checkout-shopping-cart', require('./components/CheckoutShoppingCart.vue').default);
Vue.component('items', require('./components/Items.vue').default);
Vue.component('items-b2c', require('./components/ItemsB2C.vue').default);
Vue.component('itemb2c', require('./components/ItemB2C.vue').default);
Vue.component('filter-items-field', require('./components/FilterItemsField.vue').default);
Vue.component('addresses', require('./components/Addresses.vue').default);
Vue.component('Slick');




// Define a new component called button-counter
Vue.component('button-counter', {
    data: function () {
        return {
            count: 0
        }
    },
    template: '<button v-on:click="count++">You clicked me {{ count }} times.</button>'
})


Vue.directive('focus', {

    inViewport(el) {
        var rect = el.getBoundingClientRect()
        return !(rect.bottom < 0 || rect.right < 0 ||
            rect.left > window.innerWidth ||
            rect.top > window.innerHeight)
    },

    inserted(el, binding) {
        el.$onScroll()
    },

    bind(el, binding) {

        el.classList.add('before-enter')
        el.$onScroll = function () {
            if (binding.def.inViewport(el)) {
                el.classList.add('animated')
                el.classList.add(el.getAttribute('data-transition'))
                el.classList.remove('before-enter')
                binding.def.unbind(el, binding)
            }
        }

        document.addEventListener('scroll', el.$onScroll)
    },

    unbind(el, binding) {
        document.removeEventListener('scroll', el.$onScroll)
        delete el.$onScroll
    }

});

// -----------------------------------------------------------
// app
// -----------------------------------------------------------
const app = new Vue({
    store,
    el: '#app',
    data: {
        userId: '',

        contactForm: new Form({
            subject: '',
            from: '',
            message: ''
        }),

        userForm: new Form({
            name: '',
            email: ''
        }),

        registrationForm: new Form({
            name: '',
            email: '',
            password: '',
            password_confirmation: ''
        }),

        addressForm: new Form({
            street: '',
            zip: '',
            city: ''
        }),

        // arrowRight: '<svg x="0px" y="0px" viewBox="0 0 24 24"><path d="M7.8,21c-0.3,0-0.5-0.1-0.7-0.3c-0.4-0.4-0.4-1,0-1.4l7.4-7.3L7,4.7c-0.4-0.4-0.4-1,0-1.4s1-0.4,1.4,0l8.8,8.7l-8.8,8.7C8.3,20.9,8,21,7.8,21z"/></svg>',
        slideOptions: {
            arrows: true,
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        dots: false
                    }
                },
                {
                    breakpoint: 900,
                    settings: {
                        arrows: true,
                        dots: true,
                        infinite: false
                    }
                }
            ]
        },

        newsletterForm: new Form({
            email: ''
        }),
        itemsFilter: '',
        itemsFilterKeyUpTme: null,
        showMenu: false, // fo burger on small devices
        showSubmenu: true,
        showContactForm: false,
        showUserForm: false,
        showAddressForm: true,
        showNewsletterForm: false,
        showNewsletterNotification: true,
        animatedTotal: 0
    },

    created() {

        this.animatedTotal = this.total

        window.events.$on('addToCart', item => {
            this.addToCart(item)
        });

    },

    methods: {
        handleScroll: function (event) {
            // console.log('scroll')
        },
        // ---------------------------------------------------------------
        // items
        setItems: function (query, redirect = false) {
            this.$store.commit('clearItems');
            axios.get('/items/json?' + query)
                .then(response => {
                    this.$store.commit('setItems', response.data)
                    if (redirect) window.location = '/b2c'
                });
        },
        filterItems: function (event) {
            // more then 3 charachters : TODO add some lag
            if (event.code == 'Enter') this.setItems('name=' + this.itemsFilter)
        },
        queryItems: function (event) {
            this.setItems(event.target.rel, true)
        },
        clearItems: function (event) {
            store.commit('clearItems')
        },

        // ---------------------------------------------------------------
        // cart
        addToCart: function (item) {
            // TODO: add bounce animation
            store.commit('addToCart', JSON.parse(JSON.stringify(item)))
        },
        // ---------------------------------------------------------------
        // user
        /*
        userShowForm: function (event) {
            this.showUserForm = true
        },
        */
        // ---------------------------------------------------------------
        // contact
        itemShowContact: function (event) {
            // this.contactForm.subject = event.target.rel
            this.contactForm.subject = event.target.rel;
            console.log(this.contactForm.subject)
            this.showContactForm = true
        },
        onContactSubmit() {
            this.contactForm.post('/message')
                .then(response => {
                    this.showContactForm = false;
                    // do something else
                    // alert message
                    // scroll to top
                })
                .catch(errors => {
                    flash('please fill all required fields')
                    scrollToY(0, 1500, 'easeInOutQuint');
                });
        },

        onUserSubmit() {

            this.userForm.patch('/user/' + this.userId)
                .then(response => {
                    scrollToY(0, 1500, 'easeInOutQuint');
                    flash({message: response.data.message, lvl: 'info'})
                })
                .catch(errors => {
                    scrollToY(0, 1500, 'easeInOutQuint');
                    flash('please fill all required fields')
                });
        },

        onNewsletterSubmit() {
            this.newsletterForm.post('/newsletter')
                .then(response => {
                    window.location = '/newsletter/confirmation-sent/' + response.token
                })
                .catch(errors => {
                    console.log('error')
                    // console.log(errors, 'errors');
                });
        },

        onNewsletterUnsubscribeSubmit() {
            this.newsletterForm.post('/newsletter/unsubscribe')
                .then(response => {
                    window.location = '/newsletter/unsubscribed'
                })
                .catch(errors => {
                    // console.log('error')
                    // console.log(errors, 'errors');
                });
        },

        // ---------------------------------------------------------------
        // address
        /*
        setAddresses: function () {
            axios.get('/addresses')
                .then(response => {
                    this.$store.commit('setAddresses', response.data)
                });
        },
        */
        onRegisterSubmit() {
            console.log('register');
        },
        onAddressSubmit() {

            this.addressForm.patch('/address/' + this.userId)
                .then(response => {
                    // console.log(response.data, 'onSubmit then')
                    // this.$store.commit('addPrimaryAddress', response.data)
                    // this.$store.commit('setPrimaryAddress', response.data)

                    scrollToY(0, 1500, 'easeInOutQuint');
                    flash({message: response.data.message, lvl: 'info'})
                })
                .catch(errors => {
                    // console.log('errors')
                });

        },
        toggleNewsletterRegistration() {
            this.showNewsletterNotification = false
            this.showNewsletterForm = true
        }
    },
    destroyed: function () {
        // window.removeEventListener('scroll', this.handleScroll);
    },
    computed: {
        cartQuantity() {
            return store.state.cart.length
        },
        cartContent() {
            return JSON.stringify(store.state.cart)
        },
        test() {
            console.log('test function app')
        },
        total() {
            return store.getters.total;
        },
        weight() {
            return store.getters.weight;
        },
        showCheckout() {
            return this.total > 0;
        }
    },
    watch: {
        total: function (newValue, oldValue) {
            console.log(oldValue)
            console.log(newValue)

            var vm = this

            function animate() {
                if (TWEEN.update()) {
                    requestAnimationFrame(animate)
                }
            }

            new TWEEN.Tween({tweeningNumber: oldValue})
                .easing(TWEEN.Easing.Quadratic.Out)
                .to({tweeningNumber: newValue}, 500)
                .onUpdate(function () {
                    vm.animatedTotal = this.tweeningNumber.toFixed(0)
                })
                .start()

            animate()
        }
    }
});

window.app = app
window.store = store