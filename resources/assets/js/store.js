import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist';

Vue.use(Vuex)

export const vuexLocal = new VuexPersistence ({
     storage: window.sessionStorage
    /*,
     reducer: state => ({navigation: state.navigation}),
     filter: mutation => (mutation.type == 'addNavItem')
     */
});


export const store = new Vuex.Store({
    state: {
        cart: [],
        // addresses: [],
        items: [],
        deliveryAddress: []
    },

    getters: {
        total: state => {

            let total = 0.0;

            for(let i = 0; i < state.cart.length; i++) {
                total += parseFloat(state.cart[i].price)
            }

            return total;

        },
        weight: state => {

            let total = 0.0;

            for(let i = 0; i < state.cart.length; i++) {

                let weight = parseFloat(state.cart[i].weight);

                if(weight != undefined) {
                    total += parseFloat(state.cart[i].weight)
                }
            }

            return 0;
        },
        items: state => {
            return store.state.items
        },
        /*
        addresses: state => {
            return store.state.addresses
        },
        */
        deliveryAddress: state => {
            return store.state.deliveryAddress
        },
        /*
        NOT IN MVP
        primaryAddress: state => {

            for(let available of state.addresses) {
                if(available.primary) {
                    return available
                }
            }

            return false;
        },
        */
        cart: state => {
            return store.state.cart
        }
    },

    mutations: {
        // addresses ------------------------------
        /* NOT IN MVP
        setAddresses(state, addresses) {
            state.addresses = []
            state.addresses = addresses
        },
        clearAddresses(state) {
            state.addresses = []
        },
        */
        removeAddress: (state, address) => {
            for (let key in state.addresses) {

                if(state.addresses[key].id == address.id) {
                    state.addresses.splice(key, 1)

                    axios.delete('/address/' + address.id)
                        .catch(errors => {
                            flash('Error deleting address')
                        });

                }

            }
        },
        addPrimaryAddress(state, address) {
            console.log(address, 'setPrimary')
            state.addresses.push(address)
        },
        setDeliveryAddress(state, address) {

            state.deliveryAddress = address
            /*
            NOT IN MVP
            for(let available of state.addresses) {

                if(available.primary) {
                    // persist change to db
                    available.primary = 0;
                    axios.patch('/address/' + available.id, available)
                        .then(response => {

                        })
                        .catch(errors => {

                        });
                }

                // available.primary = (available.id == address.id)
                // persist to db
                if(available.id == address.id) {
                    available.primary = 1
                    axios.patch('/address/' + address.id, address)
                }
            }
            */
        },
        /*
        NOT IN MVP
        patchAddress: function (state, address) {
            // let available = state.addresses.find(o => o.id === address.id)
            for (let key in state.addresses) {
                if(state.addresses[key].id == address.id ) {
                    state.addresses[key] = address
                }
            }
        },
        */
        // items ------------------------------
        setItems(state, items) {
            state.items = []
            state.items = items
        },
        addItem(state, item) {
            state.items.push(item)
        },
        clearItems(state) {
            state.items = []
        },


        // cart --------------------------------
        addToCart(state, item) {
            state.cart.push(item)
        },
        removeFromCart(state, index) {
            state.cart.splice(index, 1);
        },
        clearCart(state) {
            state.cart = []
        }

    },

    plugins: [vuexLocal.plugin]

});

