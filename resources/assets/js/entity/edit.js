let data = {
    entityApiKey: ''
};

let entityId = document.getElementById('entity-id').value;

axios.get('/entity/api-key/' + entityId)
    .then(function(response) {
        data.entityApiKey = response.data.api_key;
    });


const app = new Vue({
    el: '#app',
    data: data,
    methods: {
        updateApiKey: function(event) {

            // make post request to save it
            axios.post('/entity/api-key/' + entityId, {
                'api-type': document.getElementById('api-type').value,
                'api-key': document.getElementById('api-key').value
            }).then(function(response) {
                data.entityApiKey = response.data.api_key;
            });

        }
    }
});


