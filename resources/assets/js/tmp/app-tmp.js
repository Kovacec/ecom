import axios from 'axios';
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist';

Vue.use(Vuex)

const vuexLocal = new VuexPersistence ({
    storage: window.localStorage
    /*,
     reducer: state => ({navigation: state.navigation}),
     filter: mutation => (mutation.type == 'addNavItem')
     */
});


export const store = new Vuex.Store({
    state: {
        cart: [],
        items: []
    },

    getters: {

        total: state => {

            let total = 0.0;

            for(let i = 0; i < state.cart.length; i++) {
                total += parseFloat(state.cart[i].price)
            }

            return total;

        },

        weight: state => {

            let total = 0.0;

            for(let i = 0; i < state.cart.length; i++) {

                let weight = parseFloat(state.cart[i].weight);

                if(weight != undefined) {
                    total += parseFloat(state.cart[i].weight)
                }
            }

            return 0;
        },



        items: state => {

            return store.state.items;

        }
    },

    mutations: {

        setItems(state, items) {
            state.items = items
        },
        addItem(state, item) {
            state.items.push(item)
        },
        clearItems(state) {
            state.items = []
        },
        addToCart(state, item) {
            state.cart.push(item)
        },
        removeFromCart(state, index) {
            state.cart.splice(index, 1);
        },
        clearCart(state) {
            state.cart = []
        }

    },

    plugins: [vuexLocal.plugin]

})




Vue.component('flash', require('./../components/Flash.vue'));
Vue.component('modal', require('./../components/Modal.vue'));

Vue.component('cart', require('./../components/Cart.vue'));
Vue.component('checkout-steps', require('./../components/CheckoutSteps.vue'));
Vue.component('checkout-shopping-cart', require('./../components/CheckoutShoppingCart.vue'));


Vue.component('items', require('./../components/Items.vue'));
Vue.component('items-b2c', require('./../components/ItemsB2C.vue'));
Vue.component('itemb2c', require('./../components/ItemB2C.vue'));
Vue.component('filter-items-field', require('./../components/FilterItemsField.vue'));



const app = new Vue({
    store,
    el: '#app',
    data: {
        contactForm: new Form({
            subject: '',
            from: '',
            message: ''
        }),
        itemsFilter: '',
        itemsFilterKeyUpTme: null,
        showContactForm: false
    },
    created() {

        // load items
        if(store.state.items == 0) {

            axios.get('/items/json')
                .then(response => {
                    response.data.forEach(item => {
                        store.commit('addItem', item)
                    });
                });

        }

        // preload thumbnails


    },

    methods: {
        addToCart: function (item) {
            store.commit('addToCart', JSON.parse(JSON.stringify(item)))
        },
        filterItems: function(event) {

            console.log('event');

            // more then 3 charachters : TODO add some lag
            if(event.code == 'Enter') {

                this.$store.commit('clearItems');
                axios.get('/items/json?name=' + this.itemsFilter)
                    .then(response => {
                        response.data.forEach(item => {
                            this.$store.commit('addItem', item)
                        });
                    });

            }

        },
        onContactSubmit() {
            this.contactForm.post('/contact')
                .then(response => {
                    this.showContactForm = false;
                    // do something else
                });
        }

    },

    //
    computed: {
        cartQuantity() {
            return store.state.cart.length
        },
        cartContent() {
            return JSON.stringify(store.state.cart)
        },
        test() {
            console.log('test function app')
        },
        total() {
            return store.getters.total;
        },
        weight() {
            return store.getters.weight;
        }
    }



});