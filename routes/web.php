<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', 'HomeController@index')->name('home');

Route::get('/about', function() {
    return view('about');
});

Route::get('/SaaS', 'HomeController@saas');
Route::get('/box', 'HomeController@box');



/*
|--------------------------------------------------------------------------
| Checkout Steps
|--------------------------------------------------------------------------
 */

Route::get('/checkout', 'CheckoutController@index')
    ->name('checkout');

Route::get('/checkout/user-details', 'CheckoutController@userDetails')
    ->name('checkout.user-details');

Route::get('/checkout/payment', 'CheckoutController@payment')
    ->name('checkout.payment');


Route::post('/checkout/charge', 'CheckoutController@charge');
Route::get('/checkout/success', 'CheckoutController@success');


/*
|--------------------------------------------------------------------------
| Cart
|--------------------------------------------------------------------------
 */

Route::post('/cart', 'CartController@store');


/*
|--------------------------------------------------------------------------
| Address
|--------------------------------------------------------------------------
 */

Route::patch('/address/{user}', 'AddressController@update')
    ->name('address.update');

Route::get('/addresses', 'AddressController@index');
Route::post('/address', 'AddressController@store');

Route::delete('/address/{address}', 'AddressController@delete');


/*
|--------------------------------------------------------------------------
| Contact
|--------------------------------------------------------------------------
 */

Route::get('/contact', 'MessageController@index');
Route::get('/message', 'MessageController@index');


Route::post('/message', 'MessageController@store')
    ->name('message.send');


/*
|--------------------------------------------------------------------------
| Category
|--------------------------------------------------------------------------
 */

Route::get('/categories', 'CategoryController@index')->name('categories');
Route::get('/item/category/{category}', 'CategoryController@items')
    ->name('items.category');


/*
|--------------------------------------------------------------------------
| Item
|--------------------------------------------------------------------------
 */

Route::get('/item/create/entity/{entity}', 'ItemController@create')
    ->name('item.create');

Route::get('b2c', 'ItemController@b2c');

Route::get('/item/{item}', 'ItemController@show')
    ->name('item');

Route::post('item', 'ItemController@store');
Route::patch('item/{item}', 'ItemController@update');
Route::delete('item/{item}', 'ItemController@delete');


Route::get('item/edit/{item}', 'ItemController@edit')
    ->name('item.edit');

Route::get('items', 'ItemController@index')
    ->name('items');

Route::get('/items/json', 'ItemController@json')
    ->name('items.json');

/*
|--------------------------------------------------------------------------
| Entity
|--------------------------------------------------------------------------
 */
Route::get('/entity/create', 'EntityController@create')
    ->name('entity.create');

Route::post('/entity', 'EntityController@store');
Route::patch('/entity/{entity}', 'EntityController@update');

Route::get('/entities', 'EntityController@index')
    ->name('entities');

Route::get('/entity/{entity}', 'EntityController@show')->name('entity');
Route::get('/entity/edit/{entity}', 'EntityController@edit');


// TODO: refactor create entityt apiKey: when one entity have more api_keys
Route::get('/entity/api-key/{entity}', 'EntityController@apiKey');
Route::post('/entity/api-key/{entity}', 'EntityController@storeApiKey');

Route::delete('/entity/{entity}', 'EntityController@destroy');

/*
|--------------------------------------------------------------------------
| Comment
|--------------------------------------------------------------------------
 */

Route::post('/item/{item}/comment', 'CommentsController@store');


/*
|--------------------------------------------------------------------------
| Tag
|--------------------------------------------------------------------------
 */

Route::get('/items/tag/{tag}', 'TagController@index');



/*
|--------------------------------------------------------------------------
| Auth
|--------------------------------------------------------------------------
 */


Auth::routes();


/*
|--------------------------------------------------------------------------
| User
|--------------------------------------------------------------------------
 */

Route::get('/user', 'UserController@index')
    ->name('user.profile');

Route::patch('/user/{user}', 'UserController@update')
    ->name('user.update');

Route::get('/user/addresses', 'UserController@addresses')
    ->name('user.addresses');

Route::get('/user/orders', 'UserController@orders')
    ->name('user.orders');


/*
|--------------------------------------------------------------------------
| Asset
|--------------------------------------------------------------------------
 */
Route::get('/asset/{id}', 'AssetController@show');

/*
|--------------------------------------------------------------------------
| Order
|--------------------------------------------------------------------------
 */
Route::get('/orders', 'OrderController@index');


/*
|--------------------------------------------------------------------------
| Content
|--------------------------------------------------------------------------
 */
Route::get('/legal', 'ContentController@legal');
Route::get('/about', 'ContentController@about');
Route::get('/saas', 'ContentController@saas');

/*
|--------------------------------------------------------------------------
| Newsletter
|--------------------------------------------------------------------------
 */

Route::get('/newsletter', 'NewsletterController@index');

Route::post('/newsletter', 'NewsletterController@create');
Route::get('/newsletter/confirmation-sent/{token}', 'NewsletterController@confirmationSent');
Route::get('/newsletter/confirm/{token}', 'NewsletterController@confirm');

Route::get('/newsletter/unsubscribe', 'NewsletterController@unsubscribe');
Route::post('/newsletter/unsubscribe', 'NewsletterController@unsubscribe');
Route::get('/newsletter/unsubscribed', 'NewsletterController@unsubscribed');



Route::get('/cv-X2BYg235pXSDzqf', function () {
    return view('cv');
});

/*
|--------------------------------------------------------------------------
| Blog
|--------------------------------------------------------------------------
 */
Route::get('/blog', 'BlogController@index');
Route::get('/blog/1', 'BlogController@maps');

/*
|--------------------------------------------------------------------------
| Location
|--------------------------------------------------------------------------
 */
Route::get('near', 'LocationController@near');
Route::any('bounds', 'LocationController@bounds');

/*
|--------------------------------------------------------------------------
| Coin
|--------------------------------------------------------------------------
 */
Route::get('coin/rates', 'CoinController@rates');


