<?php

namespace App\Http\Middleware;

use App\Webspace;
use Closure;

class InitWebspace
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // TODO: happens after view helpers.php function
        // SO webspace can be initialized from session : I think
        // error_log('initWebspace handle');
        // get domain
        // $webspace = Webspace::find($request->getHttpHost());
        // $request->webspace = $webspace;



        return $next($request);
    }

}
