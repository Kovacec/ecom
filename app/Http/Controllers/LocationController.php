<?php

namespace App\Http\Controllers;

use function Clue\StreamFilter\fun;
use Illuminate\Http\Request;

use App\Entity;
use App\Item;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{

    /**
     * @var
     */
    protected $entityTransformer;


    /**
     * Show near items on map.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function near()
    {

        return view('near');
    }




    public function bounds(Request $request)
    {
        // create matrix for counting in sub areas
        // TODO: URL to article

        $n = 8;



        $south = $request->input('south');
        $north = $request->input('north');
        $west = $request->input('west');
        $east = $request->input('east');



        $x = [];
        $y = [];

        $length = abs($east - $west);
        $height = abs($north - $south);


        // use algorithm only if zoom level is ...

        // if($length < 10) {


            // $sql = "SELECT name, latitude, longitude from entities ";
            // $sql .= "WHERE latitude > " . $south ." AND latitude < " . $north. " AND longitude > " . $west . " AND longitude < " . $east;


            // $locations = DB::select($sql);


            /*
            return response()
                ->json([
                    'length' => $length,
                    'items' => $locations// $this->transform($items) // $this->entityTransformer->transformCollection($items->toArray()) // toArray
                ], 200);
            */

        // }

        $sql = "SELECT \n";


        $dx = $length / $n;
        $dy = $height / $n;



        $x[0] = $west;
        $y[0] = $south;

        for($i = 1; $i <= $n; $i++) {
            $x[$i] = $west + $dx * $i;
            $y[$i] = $south + $dy * $i;
        }

        $y[$n + 1] = $north;
        $x[$n + 1] = $east;

        // we hover area with n * n squares
        // we store for each square coordinates in associative array with key XY 00, 01, ..
        $sql .= "CASE \n";

        $corners = [];

        for ($i = 0; $i <= $n; $i++) {


            for($j = 0; $j <= $n; $j++) {
                $m = (string) $i . $j;
                $sql .= "WHEN longitude BETWEEN {$x[$i]} AND {$x[$i+1]} AND latitude BETWEEN {$y[$j]} AND {$y[$j+1]} THEN '{$m}' \n";

                /*
                $corners[] = [
                    'name'      => 'corner ' . $m,
                    'latitude'  => $y[$i],
                    'longitude' => $x[$j],
                    'icon'      => "/img/star-full-icon.png"
                ];
                */

            }
        }



        $sql .= "END as intervals, \n";
        $sql .= "count(*) as total, sum(longitude) as sumLongitude, sum(latitude) as sumLatitude FROM entities \n";
        $sql .= "WHERE latitude > " . $south ." AND latitude < " . $north. " AND longitude > " . $west . " AND longitude < " . $east;
        $sql .= " GROUP BY intervals";



        $groupedLocations = array_map(function($location) {
            return [
                'name'      => $location->total,
                'latitude'  => $location->sumLatitude / $location->total,
                'longitude' => $location->sumLongitude / $location->total,
                'icon'      => $location->total > 1 ? "/img/star-full-icon.png" : "/img/map-marker.png"
            ];
        }, DB::select($sql));



        /*
        $items = Entity::where('latitude', '>', $south)
            ->where('latitude', '<', $north)
            ->where('longitude', '>', $west)
            ->where('longitude', '<', $east)
            ->get();
        */



        return response()
            ->json([
                'items' => array_merge($groupedLocations, []/*,  $locations *//*, $corners*/) // $this->transform($items) // $this->entityTransformer->transformCollection($items->toArray()) // toArray
            ], 200);
    }


}
