<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Newsletter;
use App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void

    public function __construct()
    {
        $this->middleware('auth');
    }
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    public function saas() {
        return view('saas');
    }

    public function box() {
        return view('box');
    }

}
