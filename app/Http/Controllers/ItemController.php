<?php

namespace App\Http\Controllers;

use App\Entity;
use App\Http\Requests\ItemRequest;
use App\Item;
use App\Repositories\Items;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{


    public function __construct()
    {
        // $this->middleware('auth')->only(['store', 'create', 'comment']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Item $item)
    {
        $items = $item->latest()->limit(100)->get();
        return view('item.index', compact('items'));
    }


    public function json(Item $item) {

        if(request('name')) {
            return $item->where('name', 'like', '%' . request('name') . '%')->get();
        }

        if(request('category')) {
            return $item->where('category_id', '=' , request('category'))->get();
        }

        if(request('entity_id')) {
            return $item->where('entity_id', '=', request('entity_id'))->latest()->get();
        }

        // ->where('image', '<>', '')

        return $item->latest()
                    ->limit(100)
                    ->get();

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Entity $entity)
    {
        return view('item.create', compact('entity'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ItemRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {

        $entity = Entity::find(request('entity'));
        $item = Item::create(
            array_merge(
                request()->all(),
                ['entity_id' => $entity->id]
            ));


        if (request('image')) {
            $file = request()->file('image')->store('img/logo', 's3');
            $item->image = $file;
            $item->save();
        }


        /*
         *
         // TODO:
        auth()->user()->publishItem(
            new Item(request(['name', 'category', 'description']))
        );
        */

        session()->flash('message', 'Item: ' . $item->name . ' created.');
        return redirect('/entity/' . $entity->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        /*
        is_numeric($item->category_id) ?
            $categoryItems = $item->category->items()->limit(10)->get()
            : $categoryItems = [];
        */

        return view('item.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        return view('item.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $item->update($request->all());

        if (request('image')) {
            // check if path exist TODO:
            $file = request()->file('image')->store('img/logo', 's3');
            $item->image = $file;
            $item->save();
        }


        if (request('logo')) {
            $file = request()->file('image')->store('img/logo', 's3');
            // $entity->logo = $file;
            $item->image = $file;
        }


        session()->flash('message', 'Item: ' . $item->name . ' updated.');
        return redirect('/item/' . $item->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        // DELETE /items/id
    }


    public function comment(Item $item)
    {
        return redirect('/item/' . $item->id);
    }


    public function b2c()
    {
        $items = \App\Item::latest()->limit(40)->get();
        return view('b2c', compact('items'));
    }

}
