<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    public function index() {
        return view('message.index');
    }

    public function store() {
        // store contact, send email, push notification, sms ...
        $validatedData = request()->validate([
            'subject'   => 'required',
            'from'      => 'required',
            // 'body'      => 'required'
        ]);

        // Message::create($validatedData);
        return ['message' => trans('message.sent')];

    }

}
