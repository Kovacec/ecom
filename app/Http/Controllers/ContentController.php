<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function about() {
        return view('about');
    }

    public function legal() {
        return view('legal');
    }

    public function saas() {
        return view('saas');
    }
}
