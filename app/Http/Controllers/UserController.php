<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemRequest;

use App\User;

use App\Repositories\Items;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    //--------------
    // user views
    //--------------
    public function index() {
        return view('user.index', ['user' => Auth::user()]);
    }


    public function update(User $user) {

        $validatedData = request()->validate([
            'name'  => 'required',
            'email' => 'required|email',
        ]);

        $user->update(request()->all());


        return [
            'success'   => true,
            'data'      => [ 'message' => 'user.updated']
        ];

    }


    public function orders() {
        $orders = Auth::user()->orders()->get();
        return view('user.orders', compact('orders'));
    }


    public function addresses() {
        return view('user.addresses');
    }

    // TODO: user can store his payment information
    public function payment() {
        return view('user.payment');
    }


}
