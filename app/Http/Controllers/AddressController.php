<?php

namespace App\Http\Controllers;

use App\Address;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddressController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {

    }

    /**
     * Return users addresses
     * @return mixed
     */
    public function index()
    {
        return Auth::user()->addresses;
    }

    /**
     * @param User $user
     * @return bool
     * @internal param Address $address
     */
    public function update(User $user) {
        $validatedData = request()->validate([
            // 'primary' => '',
            'street' => 'required',
            'zip' => 'required',
            'city' => 'required'
        ]);

        // in MVP : todo move to addresses table
        $user->street = request()->input('street');
        $user->zip = request()->input('zip');
        $user->city = request()->input('city');

        // TODO: Address validation IN MVP
        $user->save();

        return [
            'message' => 'user.address.updated',
            'data'    => $validatedData
        ];
    }

    /**
     * @return mixed
     */
    public function store() {

        // store contact, send email, push notification, sms ...
        $validatedData = request()->validate([
            'user_id' => '',
            'name' => '',
            'street' => 'required',
            'zip' => 'required',
            'city' => 'required'
        ]);

        // TODO: get country from prefix
        // $validatedData = array_merge(
        //    $validatedData,
        //    ['country' => 'Germany']
        // );

        $address = Address::create($validatedData);

        // validate and get additional data : longitude/latitude
        $address = $this->geocode($address);
        $address->save();

        return [
            'message' => trans('address.saved'),
            'data' => $address
        ];

    }

    /**
     * https://github.com/geocoder-php/GeocoderLaravel
     * https://github.com/geocoder-php/Geocoder/tree/master/src/Common/Model
     * resolve address
     * @param \App\Address $address
     * @return array
     */
    private function geocode($address)
    {
        $q = $address->street
            . ','
            . $address->zip
            . ' '
            . $address->city
            . ',' . $address->country;


        $address->valid = false;

        if (!empty($q)) {
            $geoCodeData = app('geocoder')->geocode($q)->get();

            // interesting enough test, test, test will return an value as well some other funny words

            if (count($geoCodeData) == 1) {
                foreach ($geoCodeData as $data) {
                    if ($data instanceof \Geocoder\Provider\GoogleMaps\Model\GoogleAddress) {
                        if ($data->getLocationType() == 'ROOFTOP') {
                            $address->latitude = $data->getCoordinates()->getLatitude();
                            $address->longitude = $data->getCoordinates()->getLongitude();
                            $address->valid = true;
                        }
                    }
                }
            }
        }

        return $address;
    }

    /**
     *
     */
    function delete(Address $address) {
        $success = $address->delete();

        return  [
            'success' => $success,
            'message' => $success ? 'address removed' : 'error removing address'
        ];
    }

}


/*
} else if($data instanceof \Geocoder\Model\Address) {
        $addresses[] = [
            'locationType'      => $data->getProvidedBy(),
            'locality'          => $data->getLocality(),
            'coordinates'       => [
                'latitude'  => $data->getCoordinates()->getLatitude(),
                'longitude' => $data->getCoordinates()->getLongitude()
            ],
            'political'         => $data->getCountry()->getName()
        ];
    }
}
*/
