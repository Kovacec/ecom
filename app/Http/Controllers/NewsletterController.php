<?php

namespace App\Http\Controllers;

use App\Mail\NewsletterDOI;
use App\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


/// use Illuminate\Support\Facades\Mail;
// use Modules\Billing\Mail\NewsletterConfirmation;
// use Illuminate\Support\Facades\Input;
// use Illuminate\Support\Facades\App;
// use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


class NewsletterController
{

    public function index()
    {
        return view('newsletter.index');
    }

    /**
     * Send newsletter confirmation email.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        // check that user is human
        $validatedData = request()->validate([
            'email' => 'required|email',
        ]);

        $newsletter = Newsletter::create(
            array_merge(
                $validatedData,
                ['locale' => 'sl_SI']
            ));

        $newsletter->subscribe_at = date('Y-m-d H:i:s');
        $newsletter->subscribe_ip = $request->getClientIp();
        $newsletter->token = bin2hex(random_bytes(16));
        $newsletter->save();

        // $locale = App::getLocale();
        // $newsletter->locale = $locale;
        // Mail::to to($validatedData['email'])->send(new NewsletterConfirmation($customer));
        try {
            Mail::to($validatedData['email'])->send(new NewsletterDOI($newsletter));
        } catch (\Exception $e) {
            return response()->json(
                ['message' => $e->getMessage()],
                422
            );
        }

        return [
            'success' => 1,
            'token'   => $newsletter->token
        ];
    }


    public function confirmationSent($token)
    {
        return view('newsletter.confirmation-sent', compact($token));
    }


    /**
     * DOI
     */
    public function confirm(Request $request, $token)
    {
        $newsletter = Newsletter::where('token', '=', $token)->first();
        if ($newsletter && $newsletter->token == $token) {

            $newsletter->confirmed_at = date('Y-m-d H:i:s');
            $newsletter->confirm_ip = $request->ip();
            $newsletter->save();

            $success = true;
            $message = 'Newsletter confirmed.';
        } else {
            $success = false;
            $message = 'Not found.';
        }

        return view('newsletter.confirm', compact('success', 'message'));
    }

    public function unsubscribe(Request $request)
    {
        if ($request->isMethod('post')) {
            $validatedData = request()->validate([
                'email' => 'required|email',
            ]);

            $unsubscribed = Newsletter::where('email', '=', $validatedData['email'])
                ->update([
                    'unsubscribe_at' => date('Y-m-d H:i:s'),
                    'unsubscribe_ip' => $request->getClientIp()
                ]);

            return [
                'success' => true,
                'message' => 'OK'
            ];
        } else {
            return view('newsletter.unsubscribe');
        }
    }

    public function unsubscribed() {
        return view('newsletter.unsubscribed');
    }
}
