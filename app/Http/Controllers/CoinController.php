<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Billing\CoinPayments;

class CoinController extends Controller
{
    function rates()
    {
        $coinPayments = new CoinPayments();
        $result = $coinPayments->api_call('rates');

        // TODO: error checking
        $rates = $result['result'];

        return view('coin.rates', compact('rates'));
    }
}
