<?php

namespace App\Http\Controllers;

use App\Item;
use App\Order;
use App\ItemOrder;
use App\OrderPosition;
use Braintree\Result\Successful;
use function Clue\StreamFilter\fun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    private $cart;

    public function __construct()
    {
        // initialize cart in session
        // session(['cart' => null]);

        // Braintree_Configuration::environment('sandbox');
        // Braintree_Configuration::merchantId('use_your_merchant_id');
        // Braintree_Configuration::publicKey('use_your_public_key');
        // Braintree_Configuration::privateKey('use_your_private_key');

        $this->middleware(['auth', 'web'])
            ->except(['index', 'userDetails']);
    }


    /**
     * @return float
     */
    private function getCartTotal($cartItems) {
        return array_sum(array_map(function($cartItem) {
            return (float)$cartItem['price'];
        }, $cartItems));
    }


    /**
     * @return float|int
     */
    private function getCartTaxTotal($cartItems) {
        return array_sum(array_map(function($cartItem) {
            return (float)$cartItem['vat'] * (float)$cartItem['price'];
        }, $cartItems));
    }

    /**
     * STEP 1
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('checkout.index');
    }

    /**
     * STEP 2
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userDetails() {
        return view('checkout.user-details');
    }


    /**
     * STEP 3
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function payment() {
        // create a token
        \Braintree\Configuration::environment(config('services.braintree.environment'));
        \Braintree\Configuration::merchantId(config('services.braintree.merchant_id'));
        \Braintree\Configuration::publicKey(config('services.braintree.public_key'));
        \Braintree\Configuration::privateKey(config('services.braintree.private_key'));

        $clientToken = \Braintree\ClientToken::generate();
        return view('checkout.payment', compact('clientToken'));
    }


    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function charge(Request $request) {

        \Braintree\Configuration::environment(config('services.braintree.environment'));
        \Braintree\Configuration::merchantId(config('services.braintree.merchant_id'));
        \Braintree\Configuration::publicKey(config('services.braintree.public_key'));
        \Braintree\Configuration::privateKey(config('services.braintree.private_key'));

        $nonceFromTheClient = request('payment_method_nonce');

        // make order
        // TODO: make sanity check - data in db could be already different from client ones

        $result = \Braintree\Transaction::sale([
            'amount' => '10.00',
            'paymentMethodNonce' => $nonceFromTheClient,
            'options' => [
                'submitForSettlement' => True
            ]
        ]);


        if($result instanceof Successful) {

            // store order
            $order = new Order([
                'user_id'               => Auth::user()->id,
                'payment_type'          => '?',
                'payment_ref'           => '?',
                'total'                 => 10, // $this->getCartTotal($request->session()->get('cart-items')),
                'tax'                   => 10, // $this->getCartTaxTotal($request->session()->get('cart-items')),
                'state'                 => '?',
                'cart_token'            => '?',
                // 'delivery_' TODO: IN MVP
                // 'delivery_address_id'   => 1
            ]);


            $order->save();

            // TODO: get from cart and compare in MVP
            // get customer default address
            // $address = Auth::user()->getPrimaryAddress();
            $cartItem = session('cart-items');

            foreach ($cartItem as $cartItem) {

                $item = Item::find($cartItem['id']);

                $orderPosition = new OrderPosition([
                    'order_id'      => $order->id,
                    'item_id'       => $item->id,
                    'item_name'     => $item->name,
                    'item_price'    => $item->price,
                    'item_tax'      => $item->vat * $item->price, // REDUNDANT
                    'item_vat'      => $item->vat

                ]);

                $orderPosition->save();
            }

            return redirect('/checkout/success');

        } else {
            // redirect back and show err

            session('flash', 'Error by charging');
            return redirect('/checkout/payment');

        }


    }


    /**
     * STEP 5
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function success() {
        return view('checkout.success');
    }
}


