<?php

namespace App\Http\Controllers;

use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AssetController extends Controller
{

    public function show($id) {
        $file = File::findOrFail($id);
        return Response::make(Storage::get($file->storage_key), 200, ['Content-Type' => $file->mime_type]);
    }

}
