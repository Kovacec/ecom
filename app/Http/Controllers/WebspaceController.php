<?php

namespace App\Http\Controllers;

use App\Webspace;
use Illuminate\Http\Request;

class WebspaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Webspace  $webspace
     * @return \Illuminate\Http\Response
     */
    public function show(Webspace $webspace)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Webspace  $webspace
     * @return \Illuminate\Http\Response
     */
    public function edit(Webspace $webspace)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Webspace  $webspace
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Webspace $webspace)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Webspace  $webspace
     * @return \Illuminate\Http\Response
     */
    public function destroy(Webspace $webspace)
    {
        //
    }
}
