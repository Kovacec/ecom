<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{

    public function index(Tag $tag) {
        $items = $tag->items;
        return view('items.index', compact('items'));
    }

}
