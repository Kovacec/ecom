<?php

namespace App\Http\Controllers;

use App\Entity;
use App\Http\Requests\EntityRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class EntityController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['near', 'bounds']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->hasRole('owner')) {
            $entities = Entity::All();
        } else {
            $entities = Auth::user()->entities;
        }


        return view('entity.index', compact('entities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('entity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EntityRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntityRequest $request)
    {

        $entity = Entity::create(request()->all());

        if (request('logo')) {
            $file = request()->file('logo')->store('img/logo', 's3');
            $entity->logo = $file;

        }

        $entity->save();

        if (Auth::user()->hasRole('customer')) {
            Auth::user()->assignEntity($entity);
        }

        session()->flash('message', 'Entity: ' . $entity->name . ' created.');

        return redirect('/entities');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity $entity
     * @return \Illuminate\Http\Response
     */
    public function show(Entity $entity)
    {
        return view('entity.show', compact('entity'));
    }


    public function apiKey(Entity $entity)
    {
        return ['api_key' => $entity->api_key, $entity->api_type];
    }

    public function storeApiKey(Entity $entity)
    {
        $entity->api_key = request('api-key');
        $entity->save();

        return $entity;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity $entity
     * @return \Illuminate\Http\Response
     */
    public function edit(Entity $entity)
    {
        $data = []; // data for vue
        $date['api_key'] = $entity->api_key;
        $data['api_type'] = $entity->api_type;

        return view('entity.edit', compact('entity', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EntityRequest|Request $request
     * @param  \App\Entity $entity
     * @return \Illuminate\Http\Response
     */
    public function update(EntityRequest $request, Entity $entity)
    {
        $entity->update(request()->all());


        if (request('logo')) {
            $file = request()->file('logo')->store('img/logo', 's3');
            $entity->logo = $file;
        }

        // TODO: fire event to optimize image size


        $entity->save();


        session()->flash('message', 'Entity: ' . $entity->name . ' updated.');

        return redirect('entities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entity $entity)
    {
        session()->flash('message', 'Entity: ' . $entity->name . ' was deleted.');

        $entity->delete(); // softdelete // TODO: clear images from s3

        return redirect('/entities');
    }

}
