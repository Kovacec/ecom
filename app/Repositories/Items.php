<?php

namespace App\Repositories;

use App\Item;

class Items {

    /*
    protected  $redis;

    public function __construct(Redis $redis)
    {

    }
    */

    public function all() {
        return Item::with('tags')->all();
    }

}