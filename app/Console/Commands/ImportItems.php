<?php

namespace App\Console\Commands;

use App\Category;
use App\Entity;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class ImportItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:item';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import items for entities with webservices.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('running import for all entities');
        // $entities = Entity::where('api_key', '<>', '')->get();


        // foreach ($entities as $entity) {
            //$this->info($entity->name . ' ... ' . $entity->api_type);

            // TODO: expend to other services

            $client = new \GuzzleHttp\Client([
                'base_uri' => 'http://www.zadruga-dobrina.si'
            ]);


            $response = $client->request('GET', '/webservice/rest/object-list', [
                'query' => [
                    'apikey'       => 'caa0928eb764b6fa75673a2860832784baf311ec5ca030fe5c4126dc454e6961',
                    'order'         => 'DESC',
                    'offset'        => 0,
                    //'orderKey'      => 'id',
                    'limit'         => 1000,
                    'objectClass'   => 'item',
                    //'condition'     => ''
                ]
            ]);

            $response = json_decode($response->getBody()->getContents());

            if($response->success) {
                foreach ($response->data as $i) {


                    $response = $client->request('GET', '/webservice/rest/object/id/' . $i->id, [
                        'query' => [
                            'apikey'       => 'caa0928eb764b6fa75673a2860832784baf311ec5ca030fe5c4126dc454e6961'
                        ]
                    ]);

                    $refItem = \GuzzleHttp\json_decode($response->getBody()->getContents());
                    $published = $refItem->data->published;
                    $valid = [];

                    if($refItem->success && $published) {


                        $item = \App\Item::where('ref_id', '=', 'dobrina-' . $refItem->data->id)->first();
                        $valid[] = $refItem->data->id;

                        if($item === null) {
                            $item = new \App\Item();
                            $item->ref_id = 'dobrina-' . $refItem->data->id;
                        }

                        foreach($refItem->data->elements as $element) {

                            // TOOD:
                            switch($element->name) {
                                case 'title':
                                    $item->name = $element->value;
                                    break;
                                case 'price':
                                    $item->price = $element->value;
                                    break;
                                case 'taxRate':
                                    $item->vat = $element->value;
                                    break;
                                case 'sheme':
                                    $item->scheme = $element->value;
                                    break;
                                case 'description':
                                    $item->description = $element->value;
                                    break;
                                case 'weight':

                                    if(!empty($element->value) && is_numeric($element->value)) {
                                        $item->weight = (double)$element->value;
                                    }
                                    break;

                                case 'image':

                                    if(!empty($element->value)) {


                                        $response = $client->request('GET', '/webservice/rest/asset/id/' . $element->value, [
                                            'query' => [
                                                'apikey'       => 'caa0928eb764b6fa75673a2860832784baf311ec5ca030fe5c4126dc454e6961'
                                            ]
                                        ]);

                                        $refImage = \GuzzleHttp\json_decode($response->getBody()->getContents());

                                        // $this->info(print_r($refImage->data));
                                        $imageUrl = 'http://zadruga-dobrina.si/' . $refImage->data->path .$refImage->data->filename;


                                        $ext = '.png';
                                        $filename = $refImage->data->checksum->value . $ext;
                                        $filePath = 'dobrina/';

                                        // save to s3
                                        if(!Storage::disk('s3')->exists($filePath . $filename)) {

                                            $imagick = new \Imagick($imageUrl);
                                            $imagick->cropThumbnailImage(480, 640);
                                            // $imagick->writeImage('/test.png');
                                            $imagick->setImageFormat('png');

                                            $this->error("S3 push");
                                            Storage::disk('s3')->put($filePath . $filename, $imagick->getImageBlob());
                                        }

                                        $item->image = $filePath . $filename;

                                        //
                                        $this->info($imageUrl);

                                    }

                                    break;
                            }

                        }
                    }


                    // getCategory
                    $response = $client->request('GET', '/webservice/rest/object/id/' . $refItem->data->parentId, [
                        'query' => [
                            'apikey'       => 'caa0928eb764b6fa75673a2860832784baf311ec5ca030fe5c4126dc454e6961'
                        ]
                    ]);


                    $refCategory = \GuzzleHttp\json_decode($response->getBody()->getContents());

                    foreach($refCategory->data->elements as $element) {

                        switch ($element->name) {
                            case 'title':

                                $category = Category::find($element->value);

                                if($category == null) {
                                    $category = Category::create([
                                        'name'  => $element->value
                                    ]);

                                }


                                $item->category_id = $category->id;

                                break;
                        }

                    }


                    // set category

                    $item->user_id = 1;

                    $this->info("Save: " . $item->name . ' ' . $item->price . 'EUR ' . $item->getWeight. 'ks');
                    $item->save();

                }
            }

        // }

    }
}
