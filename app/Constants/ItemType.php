<?php

namespace App\Constants;

class ItemType
{
    const GOODS = 'goods';
    const SERVICES = 'services';

    const values = ['goods', 'services'];
}
