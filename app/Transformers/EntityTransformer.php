<?php

namespace App\Transformers;

class EntityTransformer extends Transformer
{
    public function transform($entity)
    {
        return [
            'latitude' => (float)$entity['latitude'],
            'longitude' => (float)$entity['longitude']
        ];
    }
}