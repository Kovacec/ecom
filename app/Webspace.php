<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webspace extends Model
{
    protected $fillable = [
        'locale',
        'name',
        'domain_name',
        'theme'
    ];

    /**
     * @return string
     */
    public function getQualifiedKeyName()
    {
        return 'domain_name';
    }
}
