<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    // ? should item_id be mass assigned
    protected $fillable = ['body', 'item_id'];

    public function item()
    {
        $this->belongsTo(Item::class);
    }

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
