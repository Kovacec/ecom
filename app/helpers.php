<?php

use App\Webspace;
use Illuminate\Contracts\View\Factory as ViewFactory;


if (! function_exists('view')) {
    /**
     * Get the evaluated view contents for the given view.
     *
     * @param  string  $view
     * @param  array   $data
     * @param  array   $mergeData
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    function view($view = null, $data = [], $mergeData = [])
    {
        // error_log('view helper');
        $factory = app(ViewFactory::class);

        if (func_num_args() === 0) {
            return $factory;
        }

        // get theme from request
        $webspace = Webspace::find($_SERVER['HTTP_HOST']);
        if($webspace) {
            return $factory->make('themes::'.$webspace->theme.'.'.$view, $data, $mergeData);
        } else {
            return $factory->make($view, $data, $mergeData);
        }
    }
}


