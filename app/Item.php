<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


class Item extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'entity_id',
        'name',
        'description',
        'user_id',
        'price',
        'vat',
        'ean',
        'weight',
        'stock',
        'type'
    ];


    // IMPORTANT: always use (filter only by keys)
    // http://php.net/manual/en/function.array-intersect-key.php
    private $cartRelevant = [
        'name' => 'original-name-is-missing',
        'type' => null,
        'vat' => null,
        'price' => null,
        'weight' => null,
        'ean' => null
    ];

    /**
     * Return current cart relevant item attributes.
     * @return array
     */
    public function cartRelevantAttributes()
    {
        return array_replace_recursive($this->cartRelevant,
            array_intersect_key(
                $this->toArray(), $this->cartRelevant
            )
        );
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addComment($body)
    {
        /*
        Comment::create([
            'body'      => $body,
            'item_id'   => $this->id
        ]);
        */

        // $this->comments()->create
        $this->comments()->create(compact('body'));
    }

    //public function categories()
    //{

    //return []; //$this->hasMany('App\Category');

    /*
    return array_map(function ($category) {
        return $category['category'];
    }, static::selectRaw('category')
        ->groupBy('category')
        ->get()
        ->toArray());

    */

    // }


    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }


    public function category()
    {
        return $this->belongsTo('App\Category')->withDefault([
            'no-category-set'
        ]);
    }

    public function entity()
    {
        return $this->belongsTo('App\Entity');
    }

    public function belongsToUser(User $user)
    {
        if (Auth::check() && Auth::user()->hasRole('owner')) {
            return true;
        }

        foreach ($user->entities as $entity) {
            if ($entity->id == $this->entity->id) {
                return true;
            }
        }

        return false;
    }
}
