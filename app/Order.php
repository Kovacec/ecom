<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'payment_type',
        'payment_ref',
        'state',
        'cart_token',
        'total',
        'tax',
        'state',
        'card_token',
        'delivery_address_id'
    ];

    public function positions()
    {
        return $this->hasMany(OrderPosition::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
