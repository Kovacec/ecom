<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'primary',
        'user_id',
        'name',
        'street',
        'city',
        'zip'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
