<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Entity extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'logo',
        'tel',
        'email',
        'street',
        'zip',
        'city',
        'website'
    ];

    protected $dates = ['deleted_at'];


    public function items() {
        return $this->hasMany('App\Item');
    }
}
