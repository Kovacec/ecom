<?php

namespace App\Service;

interface Service {

    public function fetch();

}