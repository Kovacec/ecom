<?php

namespace App\Providers;

use App\Tag;
use Illuminate\Support\ServiceProvider;
use App\Item;
use App\Billing\Stripe;
use Laravel\Cashier\Cashier;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        //
        \Illuminate\Support\Facades\Schema::defaultStringLength(191);

        view()->composer('layouts.partials.sidebar', function($view) {

            $categories = Item::categories();
            $tags = Tag::has('items')->pluck('name');

            $view->with(compact('categories', 'tags'));


        });


        Cashier::useCurrency('eur', '€');

        // themes
        $this->loadViewsFrom(realpath(base_path('themes')), 'themes');

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Stripe::class, function () {
            return new Stripe(config('services.stripe.secret'));
        });

        if ($this->app->environment() == 'local') {
            // $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }

    }
}
