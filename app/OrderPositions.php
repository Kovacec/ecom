<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\Item;

class OrderPosition extends Model
{
    protected $fillable = [
        'order_id',
        'item_id',
        'item_name',
        'item_price',
        'item_vat',
        'item_tax'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
