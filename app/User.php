<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return string
     */
    public function getQualifiedKeyName()
    {
        return 'email';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    /**
     * @param Item $item
     */
    public function publishItem(Item $item)
    {
        $this->items()->save($item);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function entities()
    {
        return $this->belongsToMany(Entity::class)->withTimestamps();
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasRole($name)
    {
        foreach ($this->roles as $role) {
            if ($role->name === $name) return true;
        }

        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function hasEntity($id)
    {
        foreach ($this->entities as $entity) {
            if ($entity->id === $id) return true;
        }
    }

    /**
     * @param $role
     */
    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }

    /**
     * @param $entity
     */
    public function assignEntity($entity)
    {
        return $this->entities()->attach($entity);
    }

    /**
     * @param $role
     * @return int
     */
    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }

    /**
     * @param $entity
     * @return int
     */
    public function removeEntity($entity)
    {
        return $this->entities()->detach($entity);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany(Address::class)->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function primaryAddress()
    {
        foreach ($this->addresses as $address) {
            if ($address->primary) return $address;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * Braintree user data.
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function braintree()
    {
        return $this->hasOne(Braintree::class);
    }
}
