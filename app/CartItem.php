<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = [
        'name',
        'price',
        'ean',
        'weight',
        'price',
        'qty',
        'type'
    ];
}
