<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'set app name') }}</title>
    <link rel="stylesheet" href="/css/eco.css"  />

    <!-- TODO: add to composer.json -->
    <script src="https://cdn.jsdelivr.net/npm/tween.js@16.3.4"></script>

    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/c016f43a0510ea5c1d5846ec8/cb241852600249df79f062547.js");</script>
</head>
<body>
<div id="app">
    <flash lvl="error"></flash>
    @yield ('content')
</div>

<script src="{{ asset('js/app.js') }}"></script>
@yield ('scripts')
</body>
</html>
