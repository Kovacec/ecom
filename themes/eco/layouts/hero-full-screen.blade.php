<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ekološki izdelki</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <!-- Bulma Version 0.6.0 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.0/css/bulma.min.css"
          integrity="sha256-HEtF7HLJZSC3Le1HcsWbz1hDYFPZCqDhZa9QsCgVUdw=" crossorigin="anonymous"/>
    <style>
        html, body {
            font-family: 'Open Sans', serif;
        }

        a {
            color: hsl(86, 92%, 30%);
        }

        .button.is-success, .button.is-info {
            background: hsl(86, 92%, 30%);
        }

        .button.is-info:hover, .button.is-success:hover {
            background: hsl(88, 47%, 84%);
            color: black;
        }

        .hero.is-info .title {
            // color: black;
        }

        .hero.is-info {
            background: linear-gradient(
                    rgba(0, 0, 0, 0.5),
                    rgba(0, 0, 0, 0.5)
            ), url('https://source.unsplash.com/1200x800/?eco,slovenia') no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .hero .nav, .hero.is-success .nav {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .hero .subtitle {
            padding: 3rem 0;
            line-height: 1.5;
        }
    </style>

    {{--
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
    <script>
        window.addEventListener("load", function(){
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#000",
                        "text": "#0f0"
                    },
                    "button": {
                        "background": "#0f0"
                    }
                },
                "content" : {
                    message: 'Piškotki omogočajo, da vam ponudimo svoje storitve. Z uporabo teh storitev se strinjate z našo uporabo piškotkov.'
                }
            })});
    </script>
    --}}

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114311645-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-114311645-1');
    </script>
</head>
<body>
<div id="app">
    <section class="hero is-info is-fullheight">
        <div class="hero-head">
            @include('themes::eco.layouts.partials.nav')
        </div>
        <div class="hero-body">
            @yield('content')
        </div>
    </section>


    @include('themes::eco.layouts.partials.footer')

</div>
<script type="text/javascript">
    // The following code is based off a toggle menu by @Bradcomp
    // source: https://gist.github.com/Bradcomp/a9ef2ef322a8e8017443b626208999c1
    /*
    (function () {
        var burger = document.querySelector('.nav-toggle');
        var menu = document.querySelector('.nav-menu');
        burger.addEventListener('click', function () {
            burger.classList.toggle('is-active');
            menu.classList.toggle('is-active');
        });
    })();
    */
</script>
<script src="/js/app.js"></script>
</body>
</html>
