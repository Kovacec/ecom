<footer class="footer" id="footer">
    <div class="container">
        <div class="columns">
            <div class="column is-half">
                <h2 class="background">
                    <span>
                        <a href="/items">
                            Ponudba
                        </a>
                    </span>

                    <span>
                        <a href="/saas">
                            Ponudi
                        </a>
                    </span>

                    <span>
                        <a href="/legal">
                            Splošni pogoji
                        </a>
                    </span>
                </h2>
            </div>

            <div class="column is-half has-text-centered">
                <h2 class="background">
                    <span>
                        <a class="icon" href="https://www.facebook.com/ekoloski-izdelkisi-317974155392586/" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a class="icon" href="https://twitter.com/EkoDrustvo" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a class="icon" href="https://www.instagram.com/ekoDrustvo" target="_blank">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </span>
                </h2>
            </div>
        </div>
    </div>
</footer>


<div class="bottom-line">
    <p class="has-text-centered">
        {{-- config('app.name', 'set app name') --}}<a
                href="https://www.ekoloski-izdelki.si">www.ekoloski-izdelki.si</a>
        by
        <strong>
            <a href="/about">Društvo za promocijo ekoloških izdelkov</a>
        </strong> &copy; {{ date('Y') }}
    </p>
</div>

