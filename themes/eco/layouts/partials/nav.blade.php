<nav class="navbar">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item" href="/">
                <img src="/img/g4646.png" alt="Logo">
            </a>

            <span class="navbar-item">
                <a class="button is-outlined is-small" href="mailto:info@ekoloski-izdelki.si">
                  <span class="icon">
                    <i class="fa fa-envelope"></i>
                  </span>
                  <span>info@ekoloski-izdelki.si</span>
                </a>
            </span>

            <div class="navbar-item checkout-btn">
                <transition name="fade">
                    <p class="control" v-show="showCheckout">
                        <a class="button is-primary" href="/checkout">
                            <span class="icon">
                              <i class="fa fa-shopping-basket"></i>
                            </span>
                            <span>
                                <span v-show="weight > 0">@{{ weight }} kg </span> Checkout @{{ animatedTotal }} EUR
                            </span>
                        </a>
                    </p>
                </transition>
            </div>


            <span class="navbar-burger burger"
                  style="margin-left: unset"
                  :class="{'is-active': showMenu }"
                  data-target="navbarMenu"
                  @click="showMenu = !showMenu">
              <span></span>
              <span></span>
              <span></span>
            </span>
        </div>
        <div id="navbarMenu"
             class="navbar-menu"
             :class="{'is-active': showMenu }"
             @on="showMenu">

            <div class="navbar-end">
                {{--
                <a class="navbar-item is-active">
                    Domov
                </a>
                --}}
                <div class="navbar-item">

                    {{--
                    <div class="field has-addons">
                        <div class="control">
                            <input class="input" type="text" placeholder="Find a repository">
                        </div>
                        <div class="control">
                            <a class="button is-info">
                                Search
                            </a>
                        </div>
                    </div>
                    --}}{{--
                    <div class="field">
                        <div class="control" style="display: flex">
                            <input class="input" type="text" placeholder="{{ __('Search') }}">
                            <button class="button is-primary">{{ __('Search') }}</button>
                        </div>
                    </div>--}}
                </div>

                @if (Auth::guest())
                    <div class="navbar-item">
                        <a class="" href="{{ route('login') }}">
                            Login
                        </a>
                    </div>
                    <div class="navbar-item">
                        <a class="" href="{{ route('register') }}">
                            Register
                        </a>
                    </div>
                @else
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link" href="/user">
                            {{ Auth::user()->name }}
                        </a>
                        <div class="navbar-dropdown is-right">
                            {{--
                            <a class="navbar-item" href="{{ route('user.payment') }}">
                                Payment
                            </a>
                            --}}
                            <a class="navbar-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</nav>