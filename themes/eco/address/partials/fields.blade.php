<div class="field">
    <label for="name" class="label" v-show="addressForm.street">{{ trans('address.street') }}:</label>
    <p class="control has-icons-left">
        <input type="text"
               class="input"
               id="street"
               name="street"
               placeholder="{{ trans('address.street') }}"
               v-model="addressForm.street">
        <span class="icon is-small is-left"><i class="fa fa-home"></i></span>
    </p>
    <span class="help is-danger" v-text="addressForm.errors.get('street')"></span>
</div>

<div class="field">
    <label for="zip" class="label" v-show="addressForm.zip">{{ trans('address.zip') }}</label>
    <p class="control has-icons-left">
        <input type="text"
               class="input"
               id="zip"
               name="zip"
               v-model="addressForm.zip"
               placeholder="ZIP"
               required>
        <span class="icon is-small is-left"><i class="fa fa-globe"></i></span>
    </p>
    <span class="help is-danger" v-text="addressForm.errors.get('zip')"></span>
</div>

<div class="field">
    <label for="city" class="label" v-show="addressForm.city">{{ trans('address.city') }}</label>
    <p class="control has-icons-left">
        <input type="text"
               class="input"
               id="city"
               v-model="addressForm.city"
               name="city"
               placeholder="{{ trans('address.city') }}"
               required
        >
        <span class="icon is-small is-left"><i class="fa fa-globe"></i></span>
    </p>
    <span class="help is-danger" v-text="addressForm.errors.get('city')"></span>
</div>