<form @keydown="addressForm.errors.clear($event.target.name)"
      @submit.prevent="onAddressSubmit"
      v-show="showAddressForm">
    <h2>{{ trans('address.edit.ttl') }}</h2>

    @include('themes::eco.address.partials.fields')

    <div class="field">
        <p class="control">
            <button
                    :disabled="addressForm.errors.any()"
                    :class="{'button is-primary': true, 'is-loading': addressForm.submitting }">
                {{ trans('address.update') }}
            </button>
        </p>
    </div>
</form>