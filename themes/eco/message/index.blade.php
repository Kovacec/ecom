@extends('themes::eco.layouts.hero-full-screen')

@section('content')
    <div class="section">
        <div class="container">
            <div class="columns">

                <div class="column is-half is-offset-one-quarter box">
                    <h1 class="title">{{-- trans('message.contact.title') --}}Kontakt</h1>
                    <form class="form-horizontal"
                          method="POST"
                          action="{{ route('message.send') }}"
                          @submit.prevent="onContactSubmit"
                          @keydown="contactForm.errors.clear($event.target.name)">
                          @include('message.partials.inputs')
                    <button :class="{'button is-success': true, 'is-loading': contactForm.submitting }"
                            :disabled="contactForm.errors.any()">{{-- trans('message.send')--}}Pošlji</button>

                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
