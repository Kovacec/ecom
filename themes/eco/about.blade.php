@extends('themes::eco.layouts.master')

@section('content')
    @include('themes::eco.layouts.partials.nav')
    <div class="container has-text-centered">
        <div class="column is-6 is-offset-3">
            <h1 class="subtitle">
                Društvo za promocijo ekoloških izdelkov
            </h1>
            <h2 class="title">V pripravi.</h2>
        </div>
    </div>
    @include('themes::eco.layouts.partials.footer')
@endsection
