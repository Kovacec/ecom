@extends('themes::eco.layouts.master')

@section('content')

    @include('themes::eco.layouts.partials.nav')


    <div id="page">


        <section class="section container features" v-focus data-transition="fadeInaw">
            <div class="columns">
                <div class="column is-half">
                    <div class="card">
                        <div class="card-content has-text-centered">
                            <strong>Brezplacen racun</strong>
                            <p>Brezplacno ustvarite racun in ponudite svoje izdelke na spletu s par kliki.</p>
                        </div>
                    </div>
                </div>
                <div class="column is-half">
                    <i class="fa fa-group"></i>
                </div>
            </div>
        </section>



        <section class="section container">
            <div class="columns">
                <div class="column is-half" v-focus data-transition="bounceInLeft">
                        VIDEO
                </div>
                <div class="column is-half" v-focus data-transition="bounceInRight">
                    <div class="card">
                        <div class="card-content">
                            Enostaven vmesnik. Deluje na vseh napravah vse kar potrebujete je povezava z internetom.
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="section container">
            <div class="columns">
                <div class="column is-half" v-focus data-transition="bounceInLeft">
                    <div class="card">
                        <div class="card-content">
                            Uporabniki lahko enostavno narocijo vas izdelek. Uporabijo lahko vrsto placilnih sredstev.
                        </div>
                    </div>
                </div>
                <div class="column is-half" v-focus data-transition="bounceInRight">
                    CHECKOUT
                </div>
            </div>
        </section>

        <section class="section container">
            <div class="columns">
                <div class="column is-half" v-focus data-transition="bounceInLeft">
                    <div class="card">
                        <div class="card-content">
                            O novem narocilu vas obvestimo preko sms-a, emaila. V uporabniskem vmesniku lahko natisnete podatke in oddate izdelek na posto.
                        </div>
                    </div>
                </div>
                <div class="column is-half" v-focus data-transition="bounceInRight">

                </div>
            </div>
        </section>

        <section class="section container">
            <div class="columns">
                <div class="column is-half" v-focus data-transition="bounceInLeft">
                    <div class="card">
                        <div class="card-content">
                            Ko ponudbnik dobi svojo posiljko se vam nakeza denar. Mala provizija se placa za stroske nakupa za placilo s kreditno kartico ter promocijo.
                        </div>
                    </div>
                </div>
                <div class="column is-half" v-focus data-transition="bounceInRight">
                    IMAGE DELIVERY
                </div>
            </div>
        </section>


        <section class="section container">
            <div class="columns">
                <div class="column is-half" v-focus data-transition="bounceInLeft">
                    <div class="card">
                        <div class="card-content">
                            Vsak uporabnik je lahko tudi promotor. Za vsak izdelek dobi referencno povezavo. Ce proda izdelek dobi 10% od nakupa.
                        </div>
                    </div>
                </div>
                <div class="column is-half" v-focus data-transition="bounceInRight">
                </div>
            </div>
        </section>
    </div>

    @include('themes::eco.layouts.partials.footer')

@endsection

@section('scripts')

@endsection