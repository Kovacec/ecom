<form class="form-horizontal"
      @submit.prevent="onUserSubmit"
      @keydown="userForm.errors.clear($event.target.name)"
>
    <h2>{{ trans('user.profile.ttl') }}</h2>
    <div class="field">
        <label for="name" class="label">{{ trans('user.name') }}:</label>
        <p class="control has-icons-left">
            <input type="text"
                   class="input"
                   id="name"
                   name="name"
                   placeholder="{{ trans('user.name') }}"
                   v-model="userForm.name">
            <span class="icon is-small is-left"><i class="fa fa-user"></i></span>
        </p>
        <span class="help is-danger" v-text="userForm.errors.get('name')"></span>
    </div>

    <div class="field">
        <label for="email" class="label">{{ trans('user.email') }}:</label>
        <p class="control has-icons-left">
            <input type="text"
                   class="input"
                   id="email"
                   name="email"
                   placeholder="{{ trans('user.email') }}"
                   v-model="userForm.email">
            <span class="icon is-small is-left"><i class="fa fa-envelope"></i></span>
        </p>
        <span class="help is-danger" v-text="userForm.errors.get('email')"></span>
    </div>

    <div class="field">
        <div class="control">
            <button :class="{'button is-success': true, 'is-loading': userForm.submitting }"
                    :disabled="userForm.errors.any()">{{ trans('user.update') }}</button>

        </div>
    </div>
</form>
