@extends('themes::eco.layouts.master')

@section('content')
    @include('themes::eco.layouts.partials.nav')
    <div class="section">
        <div class="container">
            <div class="columns">
                @include('user.partials.sidebar')
                <div class="column">
                    <div class="notification">
                        <addresses></addresses>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('themes::eco.layouts.partials.footer')
@endsection
