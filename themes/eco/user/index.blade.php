@extends('themes::eco.layouts.master')

@section('content')
    @include('themes::eco.layouts.partials.nav')
    <div class="section">
        <div class="container">
            <div class="columns">
                @include('user.partials.sidebar')
                <div class="column">
                    <div class="content">
                        @include('themes::eco.user.partials.edit')
                        @include('themes::eco.address.partials.edit')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('themes::eco.user.edit-profile-modal')
    @include('themes::eco.layouts.partials.footer')
@endsection


@section('scripts')

    <script>
        app.userId = {{ Auth()->user()->id }}
        app.userForm.name = '{{ Auth()->user()->name }}';
        app.userForm.email = '{{ Auth()->user()->email }}';
        app.addressForm.street = '{{ Auth()->user()->street }}';
        app.addressForm.zip = '{{ Auth()->user()->zip }}';
        app.addressForm.city = '{{ Auth()->user()->city }}';
        //app.setAddresses() not in MVP
    </script>

@endsection