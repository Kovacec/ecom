{{-- Contact form --}}
<form class="form-horizontal"
      method="POST"
      action="{{ route('message.send') }}"
      @submit.prevent="onEditUserSubmit"
      @keydown="editUserForm.errors.clear($event.target.name)"
>
    {{ csrf_field() }}

    {{--
    <modal v-show="showEditUserForm" @close="showEditUserForm = false">
        <template slot="header">{{ trans('contact.us') }}</template>
        <template slot="content">
            @include('resources.views.message.partials.inputs')
        </template>
        <template slot="footer">
            <button :class="{'button is-success': true, 'is-loading': editUserForm.submitting }"
                    :disabled="editUserForm.errors.any()">{{ trans('user.update') }}</button>
            <button class="button" @click="showEditUserForm = false">Cancel</button>
        </template>
    </modal>
    --}}

</form>