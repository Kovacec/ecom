@extends('themes::eco.layouts.master')

@section('content')
    @include('themes::eco.layouts.partials.nav')
    <div class="container has-text-centered">
        <div class="column is-6 is-offset-3">
            <h1 class="title">
                Splošni pogoji
            </h1>
            <h2 class="subtitle">E-novice</h2>
            <p>S prijavo dovoljujete spletnemu mestu ekoloski-izdelki.si obdelovanje posredovanih osebnih podatkov (elektonskega naslova). Vaš elektonski naslov bomo uporabljali izključno za obveščanje o novostih in zanimivostih s portala ekoloski-izdelki.si. Od elektronskih novic se lahko kadarkoli odjavite na naslovu <a href="{{ url('/') }}/newsletter/unsubscribe">{{ url('/') }}/newsletter/unsubscribe</a> ali nam pišete na elektonski naslov <a href="mailto:info@ekoloski-izdelki.si">info@ekoloski-izdelki.si</a>.</p>
        </div>
    </div>
    @include('themes::eco.layouts.partials.footer')
@endsection
