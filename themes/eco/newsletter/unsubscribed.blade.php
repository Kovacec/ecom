@extends('themes::eco.layouts.hero-full-screen')

@section('content')
    <div class="container has-text-centered">
        <div class="column is-6 is-offset-3">
            <h1 class="title">
                Odjavljeni
            </h1>
            {{--
            <h2 class="subtitle">
                Na vaso elektronsko posto smo vam poslali potrditveno sporocilo. Kliknite na link v elektronskem sporocilu za dokoncanje prijave.
            </h2>
            --}}
        </div>
    </div>
@endsection
