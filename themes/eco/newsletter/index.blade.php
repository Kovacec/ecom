@extends('themes::eco.layouts.hero-full-screen')

@section('content')
    <div class="container has-text-centered">
        <div class="column is-6 is-offset-3">
            <h1 class="title">
                kmalu
            </h1>
            <h2 class="subtitle">
                Spletna trgovina z ekolosko ponudbo in portal z informacijami.
            </h2>
            <div class="box">

                <form
                        method="POST"
                        action="/newsletter"
                        @submit.prevent="onNewsletterSubmit"
                        @keydown="newsletterForm.errors.clear($event.target.name)"
                        id="newsletterForm">

                    <div class="field is-grouped">
                        <p class="control is-expanded">
                            <input :class="{'input': true, 'is-danger': newsletterForm.errors.has('email') }"
                                   type="email"
                                   id="email"
                                   name="email"
                                   disabled
                                   v-model="newsletterForm.email"
                                   placeholder="Vpišite vaš e-mail naslov">
                            <span class="help is-danger" v-text="newsletterForm.errors.get('email')"></span>
                        </p>
                        <p class="control">
                            <button :class="{'button is-info': true, 'is-loading': newsletterForm.submitting }"
                                    :disabled="newsletterForm.errors.any()" type="submit" form="newsletterForm">
                                Obveščaj me
                            </button>
                        </p>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
