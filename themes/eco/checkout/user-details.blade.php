@extends('themes::eco.layouts.master')

@section('content')

    @include('themes::eco.layouts.partials.nav-checkout')

    <div class="container">
        <div class="section">
            <checkout-steps step="user-details"
                            next="/checkout/payment"
                            next-text="Payment"
                            prev="/checkout"
                            prev-text="Shopping Cart"
                            type="{{ Auth::check() ? 'standard' : 'guest' }}"
                            :registration-form="registrationForm"
                            :address-form="addressForm">
            </checkout-steps>
        </div>
        {{--
        <div class="section">
            <checkout-shopping-cart step="shopping-cart"></checkout-shopping-cart>
        </div>
        --}}
        <div class="section">
            <div class="columns">
                <div class="column is-half is-offset-one-quarter">
                    @if(!Auth::check())
                        <h2 class="title">Already have an account?</h2>
                        <a href="/login?redirect=/checkout/user-details" class="button is-info">Login</a>
                        <hr>
                        <h2 class="title">
                            <small><i>or</i></small>
                            create an account
                        </h2>
                        @include('auth.partials.register-fields')
                    @endif
                    @if(Auth::user())
                        <p>
                            <small><i>{{ Auth::user()->email }}</i></small>
                        </p>
                        <h1 class="title">{{ Auth::user()->name }}</h1>
                        <div class="section text-is-center">
                            {{--
                                todo: one to many : address
                                <addresses></addresses>
                            --}}
                            @include('themes::eco.address.partials.fields')
                        </div>

                    @endif

                </div>
            </div>
        </div>

        <div class="section">
            <checkout-shopping-cart step="user-details"></checkout-shopping-cart>
        </div>
    </div>

    <modal v-show="showContactForm" @close="showContactForm = false">
        <template slot="header">{{ trans('checkout.address.validation') }}</template>
        <template slot="content">
        </template>
        <template slot="footer">
            <button :class="{'button is-success': true }">{{ trans('address.accept') }}</button>
            <button class="button" @click="showContactForm = false">Cancel</button>
        </template>
    </modal>

@endsection


@section('scripts')
    @if(Auth::check())
        <script>
            app.userId = {{ Auth()->user()->id }}
            app.addressForm.street = '{{ Auth()->user()->street }}'
            app.addressForm.zip = '{{ Auth()->user()->zip }}'
            app.addressForm.city = '{{ Auth()->user()->city }}'
        </script>
    @endif
@endsection