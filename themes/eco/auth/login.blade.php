@extends('themes::eco.layouts.master')

@section('content')

    <section class="hero is-fullheight">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-half-desktop is-offset-one-quarter-desktop">
                        <h1 class="title">{{ trans('auth.login') }}</h1>
                        <hr>
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="redirectTo" value="{{ $redirectTo }}" />
                            <div class="field">
                                <label class="label" for="email">{{ trans('auth.email') }}</label>
                                <input id="email"
                                       type="email"
                                       class="input{{ $errors->has('email') ? ' is-danger' : '' }}"
                                       name="email"
                                       placeholder="{{ trans('auth.email') }}"
                                       value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <p class="help is-danger">
                                        {{ $errors->first('email') }}
                                    </p>
                                @endif
                            </div>
                            <div class="field{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="label">{{ trans('auth.password') }}</label>
                                <input id="password"
                                       type="password"
                                       class="input"
                                       name="password"
                                       placeholder="{{ trans('auth.password') }}"
                                       required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="field">
                                <div class="control">
                                    <label class="checkbox">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        {{ trans('auth.remember_me') }}
                                    </label>
                                </div>
                            </div>
                            <div class="field is-grouped">
                                <div class="control">
                                    <button type="submit" class="button is-primary">
                                        {{ trans('auth.submit') }}
                                    </button>
                                </div>
                                <div class="control">
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ trans('auth.forgot_your_password') }}
                                    </a>
                                </div>
                            </div>
                        </form>
                        {{--
                        <hr>
                        <p class="has-text-centered">
                            continue with facebook <br>
                            continue with google <br>
                        </p>
                        --}}
                        <hr>
                        <p class="has-text-centered">
                            <a href="/">www.ekoloski-izdelki.si</a>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
