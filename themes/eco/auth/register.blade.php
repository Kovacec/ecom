@extends('themes::eco.layouts.master')

@section('content')
    <section class="hero is-fullheight">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-half-desktop is-offset-one-quarter-desktop">
                        <h1 class="title">{{ trans('auth.register') }}</h1>
                        <hr>
                        <form class="form-horizontal"
                              method="POST"
                              @submit.prevent="onRegisterSubmit"
                              action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="field">
                                <label for="name" class="label">Name</label>
                                <input id="name"
                                       type="text"
                                       class="{'input': true, 'has-danger': registrationForm.errors.get('name')}"
                                       name="name"
                                       v-model="registrationForm.name"
                                       autofocus
                                >
                                <span class="help is-danger" v-text="registrationForm.errors.get('name')"></span>
                            </div>
                            <div class="field">
                                <label for="email" class="label">E-Mail Address</label>
                                <input id="email"
                                       type="email"
                                       class="{'input': true, 'has-danger': registrationForm.errors.get('email')}"
                                       name="email"
                                       v-model="registrationForm.email"
                                >
                                <span class="help is-danger" v-text="registrationForm.errors.get('email')"></span>
                            </div>
                            <div class="field{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="label">Password</label>
                                <input id="password"
                                       type="password"
                                       class="{'input': true, 'has-danger': registrationForm.errors.get('name')}"
                                       v-model="registrationForm.password"
                                       name="password">
                                <span class="help is-danger" v-text="registrationForm.errors.get('password')"></span>
                            </div>
                            <div class="field">
                                <label for="password-confirm" class="label">Confirm Password</label>
                                <input id="password-confirm"
                                       type="password"
                                       class="{'input': true, 'has-danger': registrationForm.errors.get('password_conformation')}"
                                       v-model="registrationForm.password_confirmation"
                                       name="password_confirmation">
                            </div>
                            <div class="field">
                                <div class="control">
                                    <button type="submit" class="button is-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                        <hr>
                        <p class="has-text-centered">
                            <a href="/">www.ekoloski-izdelki.si</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
