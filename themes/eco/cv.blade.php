<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bojan Kovačec</title>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="/css/app.css"/>

    <style>
        body {
            background: white;
        }
    </style>

</head>

<body>

<div class="container is-fluid">
    <div class="columns  is-gapless forced-gapless">

        <div class="column is-3-desktop is-4-tablet">

            <div class="section">
                <figure class="image is-square">
                    <img src="https://s.gravatar.com/avatar/63e14fb65602a9187e435b713cee66eb?s=320">
                </figure>

                <div>
                    <h1 class="title">Bojan Kovačec</h1>
                    <h2 class="subtitle">Full Stack Developer</h2>
                </div>

                <div>
                    <a href="https://www.linkedin.com/in/bojankovacec" class="icon-text">
                        <span class="icon is-small">
                          <i class="fa fa-linkedin"></i>
                            <!-- LinkedIN -->
                        </span> bojankovacec
                    </a>
                </div>

            </div>

        </div>

        <div class="column is-9-desktop is-8-tablet">
            <div class="section">
                <h1 class="title">Introduction</h1>
                <p>I am Bojan Kovačec born 03.04.1983 in Ptuj, Slovenia. </p>
            </div>

            <div class="section">
                <h2 class="subtitle">Education</h2>

                <div class="timeline">
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <p class="heading">1990 Primary School</p>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <p class="heading">1998 High School</p>
                            <p>Gymnasium Ptuj</p>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <p class="heading">2002 Faculty of Electrical Engineering and Computer Science</p>
                            <p>Software</p>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <p class="heading">2009 finished 9. semester (did not finish: master thema Eco Food
                                Delivery: http://http://zadruga-dobrina.si)</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section">
                <h2 class="subtitle">Work experience</h2>

                <div class="timeline">
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <p class="heading">2003 Help Desk University For Econimics Maribor</p>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <p class="heading">2005 Gazela d.o.o Web Developer</p>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <p class="heading">2006 Imagine Accounting Software developer</p>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <p class="heading">Various projects, worked on own or in different free lance teams</p>
                            <p></p>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <p class="heading">2012 Loewe Opta Gmbh CRM developer</p>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <p class="heading">2014 Loewe Technologies GmbH CRM/Retail Analytics</p>
                        </div>
                    </div>
                </div>
                <br><br>
                <div>
                    I am employed at Loewe Technologies GmbH. I started to work for companies after my first year of
                    study.
                    I studied software where I got good knowledge in software principles like 3D graphics, artificial
                    intelligence, building a compiler and similar but
                    in my real working experience I was always working as mostly as web developer (back end and front
                    end), since there was most of work opportunities in this field for a free lancer.
                </div>
            </div>

            <div class="section">
                <h2 class="subtitle">Some references</h2>
                <ul>
                    <li><a href="http://zadruga-dobrina.si">[] http://zadruga-dobrina.si</a></li>
                    <li><a href="http://www.noreponudbe.si">[] http://www.noreponudbe.si</a></li>
                    <li><a href="http://roglab.si">[2010] http://roglab.si</a></li>
                    <li><a href="https://www.loewe.tv">[] https://www.loewe.tv</a></li>
                </ul>
            </div>

            <div class="section">
                <h2 class="subtitle">
                    Skills
                </h2>

                <span>PHP (PHP 7)</span>
                <progress class="progress is-primary" value="90" max="100"></progress>
                <span>Linux administration</span>
                <progress class="progress is-primary" value="70" max="100"></progress>
                <span>Java</span>
                <progress class="progress is-primary" value="70" max="100"></progress>
                <span>Javascript (es6)</span>
                <progress class="progress is-primary" value="60" max="100"></progress>
                <span>CSS (less, sass)</span>
                <progress class="progress is-primary" value="70" max="100"></progress>
                <span>Python</span>
                <progress class="progress is-primary" value="50" max="100"></progress>
                <span>AWS</span>
                <progress class="progress is-primary" value="30" max="100"></progress>
                <span>Zend Framework</span>
                <progress class="progress is-primary" value="90" max="100"></progress>
                <span>Pimcore</span>
                <progress class="progress is-primary" value="90" max="100"></progress>
                <span>Laravel</span>
                <progress class="progress is-primary" value="30" max="100"></progress>
                <span>Vuejs</span>
                <progress class="progress is-primary" value="30" max="100"></progress>
            </div>
        </div>

    </div>

</div>

</body>

</html>