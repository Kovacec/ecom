@extends('themes::eco.layouts.master')

@section('content')

    @include('themes::eco.layouts.partials.nav')
    <section class="section notification is-danger">
        <h1>Spletna stran je v izdelavi.</h1>
    </section>

    <section class="section">
        {{--   :prevArrow="arrowLeft" :nextArrow="arrowRight" --}}
        <agile :options="slideOptions">
            <div class="slide slide--1 container has-text-centered">
                <div>
                    <h3 class="title">Drustvo za promocijo ekoloskih izdelkov</h3>
                    <h4 class="subtitle">Digitalizacija ponudbe ekoloških izdelkov</h4>
                </div>
            </div>
            <div class="slide slide--2 container has-text-centered">
                <h3 class="title">Ponudba izdelkov</h3>
                <h4 class="subtitle">
                    <a href="/items">Napreden sikalnik produktov</a>,
                    <a href="/nearby">Iskanje Lokalnik produktov</a>,
                    <a href="/delivery">'Ekoloska' dostava</a>
                </h4>
            </div>
            <div class="slide slide--3 container has-text-centered">
                <h3 class="title">Ponudi svoje izdelke</h3>
                <h4 class="subtitle">
                    <a href="/saas">Sami ponudite svoje izdelke</a>,
                    <a href="/blog">Blog</a>,
                    <a href="/contact">Kontakt</a>
                </h4>
            </div>
        </agile>
    </section>

    <section class="section container">
        <div class="columns features">
            <div class="column is-6 has-text-centered">
                <a class="" href="/items">
                    <div style="font-size:3em; color:Tomato">
                        <i class="fa fa-shopping-basket"></i>
                    </div>
                    Ponudba
                </a>
            </div>
            <div class="column is-6 has-text-centered">
                <a class="" href="/saas">
                    <div style="font-size:3em; color:Tomato">
                        <i class="fa fa-user"></i>
                    </div>
                    Ponudi
                </a>
            </div>
        </div>
    </section>

    <section class="hero is-medium bg1">
        <div class="hero-body">
            <div class="column is-6 is-offset-3">
                <div class="box" v-focus data-transition="flipInX">
                    <form
                            method="POST"
                            action="/newsletter"
                            @submit.prevent="onNewsletterSubmit"
                            @keydown="newsletterForm.errors.clear($event.target.name)"
                            id="newsletterForm">
                        <div class="field is-grouped">
                            <p class="control is-expanded">
                                <input :class="{'input': true, 'is-danger': newsletterForm.errors.has('email') }"
                                       type="email"
                                       id="email"
                                       name="email"
                                       v-model="newsletterForm.email"
                                       placeholder="Vpišite e-mail">
                                <span class="help is-danger" v-text="newsletterForm.errors.get('email')"></span>
                            </p>
                            <p class="control">
                                <button :class="{'button is-info': true, 'is-loading': newsletterForm.submitting }"
                                        :disabled="newsletterForm.errors.any()" type="submit" form="newsletterForm">
                                    Obveščaj me
                                </button>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    @include('themes::eco.layouts.partials.footer')
@endsection


@section('scripts')
    <script>

    </script>
@endsection