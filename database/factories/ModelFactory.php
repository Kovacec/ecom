<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*
|--------------------------------------------------------------------------
| Address
|--------------------------------------------------------------------------
*/

use App\Constants\ItemType;

$factory->define(App\Address::class, function (Faker\Generator $faker) {
    return [
        'street' => $faker->streetAddress(),
        'city' => $faker->city(),
        'zip' => $faker->postcode(),
        'latitude' => $faker->latitude(),
        'longitude' => $faker->longitude()
    ];
});

/*
|--------------------------------------------------------------------------
| User
|--------------------------------------------------------------------------
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {

    static $password;
    $localisedFaker = \Faker\Factory::create("sl_SI");

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'street' => $localisedFaker->streetAddress(),
        'zip' => $localisedFaker->postcode(),
        'city' => $localisedFaker->city(),
        'latitude' => $localisedFaker->latitude(),
        'longitude' => $localisedFaker->longitude()
    ];

});

/*
|--------------------------------------------------------------------------
| Tag
|--------------------------------------------------------------------------
*/
$factory->define(App\Tag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->word,
    ];
});

/*
|--------------------------------------------------------------------------
| Item
|--------------------------------------------------------------------------
*/
$factory->define(App\Item::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word(),
        'entity_id' => 1,
        // 'category' => $faker->word(),
        'user_id' => 1,
        'weight' => rand(10, 10000),
        'type' => ItemType::GOODS,
        'price' => (double)rand(1, 2000),
        'vat' => 20,
        'description' => $faker->paragraph()
    ];
});

/*
|--------------------------------------------------------------------------
| CartItem
|--------------------------------------------------------------------------
*/
$factory->define(App\CartItem::class, function () {
    return factory(App\Item::class)->make()->cartRelevantAttributes();
});

/*
|--------------------------------------------------------------------------
| Entity
|--------------------------------------------------------------------------
*/

$factory->define(App\Entity::class, function (Faker\Generator $faker) {

    $localisedFaker = Faker\Factory::create("sl_SI");
    return [
        'name' => $localisedFaker->word(),
        // 'category' => $faker->word(),
//        'user_id' => 1,
//        function () {
//            return factory(App\User::class)->create()->id;
//        },
        'street' => $localisedFaker->streetAddress(),
        'zip' => $localisedFaker->postcode(),
        'city' => $localisedFaker->city(),
        'latitude' => $localisedFaker->unique()->numberBetween(46700, 46200) / 1000,
        'longitude' => $localisedFaker->unique()->numberBetween(15000, 16000) / 1000,
        'description' => $localisedFaker->paragraph(),
    ];
});


/*
|--------------------------------------------------------------------------
| Webspace
|--------------------------------------------------------------------------
*/
$factory->define(App\Webspace::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->word(),
        'domain_name' => $faker->domainName()
    ];

});


/*
|--------------------------------------------------------------------------
| Property
|--------------------------------------------------------------------------
*/
$factory->define(App\Property::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->word(),
    ];

});

