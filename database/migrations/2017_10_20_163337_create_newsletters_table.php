<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewslettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletters', function (Blueprint $table) {
            $table->increments('id');

            $table->string('email');
            $table->string('locale');
            // $table->boolean('is_active')->default(false);

            // newsletter registration
            $table->string('token')->nullable();
            $table->timestamp('subscribe_at')->nullable();
            $table->string('subscribe_ip')->nullable();

            // newsletter confirmation DOI
            $table->boolean('confirmed')->nullable();
            $table->timestamp('confirmed_at')->nullable();
            $table->string('confirm_ip')->nullable();

            // newsletter unsubscription
            $table->timestamp('unsubscribe_at')->nullable();
            $table->string('unsubscribe_ip')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletters');
    }
}
