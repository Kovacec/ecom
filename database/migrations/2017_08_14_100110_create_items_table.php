<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->softDeletes();
            $table->increments('id');
            $table->boolean('published')->default(false);
            $table->enum('type', ['goods', 'services']);
            $table->string('ref_id')->nullable(); // if item is imported from other system
            // https://www.gs1.org/barcodes/ean-upc
            $table->string('EAN', 13)->nullable();  // tmp
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->double('price');
            $table->double('vat');
            $table->string('scheme')->nullable(); // eko, po, dd, ip
            $table->integer('weight');
            $table->integer('stock')->nullable()->default(0);
            $table->integer('entity_id')->unsigned()->index();
            $table->foreign('entity_id')->references('id')->on('entities')->onDelete('restrict');
            // there has to be info which user changed it
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }

}
