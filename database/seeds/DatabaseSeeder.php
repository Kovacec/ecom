<?php

use Illuminate\Database\Seeder;

use Table\AddressesTableSeeder;
use Table\EntitiesTableSeeder;
use Table\ItemsTableSeeder;
use Table\OrderPositionsTableSeeder;
use Table\OrdersTableSeeder;
use Table\RoleTableSeeder;
use Table\UsersTableSeeder;

use Table\WebspacesTableSeeder;

class DatabaseSeeder extends Seeder
{
    private $tables = [
        'webspaces' => WebspacesTableSeeder::class,
        'users' => UsersTableSeeder::class,
        // 'addresses' => AddressesTableSeeder::class,
        // 'address_user' => AddressUserTableSeeder::class,
        'entities' => EntitiesTableSeeder::class,
        'items' => ItemsTableSeeder::class,
        'roles' => RoleTableSeeder::class,
        'orders' => OrdersTableSeeder::class,
        // 'order_positions' => OrderPositionsTableSeeder::class //
    ];


    /**
     * Clean all data in database.
     *
     * @return void
     */
    private function cleanDatabase()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        foreach ($this->tables as $tableName => $seederClass) {
            DB::table($tableName)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }


    /**
     * Seed database.
     *
     * @return void
     */
    private function seedDatabase()
    {
        foreach ($this->tables as $tableName => $seederClass) {
            $this->call($seederClass);
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->cleanDatabase();
        $this->seedDatabase();
    }
}

