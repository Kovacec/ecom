<?php

use Illuminate\Database\Seeder;

class PropertyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // TODO: move to noSql
        DB::table('properties')->delete();

        \App\Property::create([
            'name'  => 'latitude'
        ]);

        \App\Property::create([
            'name'  => 'longitude'
        ]);

    }
}
