<?php

namespace Table;

use App\Webspace;
use Illuminate\Database\Seeder;

class WebspacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Webspace::create([
            'locale'        => 'sl_SI',
            'name'          => 'ekoloski izdelki',
            'domain_name'   => 'ekoloski-izdelki.local',
            'theme'         => 'eco'
        ]);

        Webspace::create([
            'locale'        => 'en',
            'name'          => 'ecom.locate.tips',
            'domain_name'   => 'ecom.locate.tips',
            'theme'         => 'eco'
        ]);
    }
}
