<?php

use Illuminate\Database\Seeder;

class WebspaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Webspace::create([
            'name' => 'ekoloski izdelki',
            'domain_name' => 'ekolosko.local',
            'locale' => 'sl_SI',
            'theme' => 'eco'
        ]);

    }

}
