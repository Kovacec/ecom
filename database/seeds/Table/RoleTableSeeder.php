<?php

namespace Table;

use App\Entity;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $member = Role::create([
            'name'  => 'member'
        ]);

        $owner = Role::create([
            'name' => 'owner'
        ]);

        $customer = Role::create([
            'name' => 'customer'
        ]);

        // User::find('bojan.kovacec@gmail.com')->rolfactory(Entity::class, 10)->create())->attach($owner);
        // User::find('test@example.com')->roles()->attach($member);
        // User::find('customer@example.com')->roles()->attach($customer);
    }
}

