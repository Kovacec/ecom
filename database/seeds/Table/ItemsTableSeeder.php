<?php

namespace Table;

use Illuminate\Database\Seeder;

use App\Item;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('items')->delete();
        factory(Item::class, 100)->create();
        // $items = Item::all();

        // set tags
        /*
        foreach ($items as $item) {

            for($i = 0; $i < rand(1, 10); $i++) {
                $item->tags()->attach($i);
            }

        }
        */


    }
}
