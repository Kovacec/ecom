<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('item_tag')->truncate();
        // DB::table('tags')->truncate();
        foreach (factory(\App\Tag::class, 20) as $tag) {
            $tag->save();
        }
    }
}
