<?php

use App\Address;
use App\User;
use Illuminate\Database\Seeder;

class AddressUserTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        $userIds = User::pluck('id');
        $addressIds = Address::pluck('id');

        foreach (range(1, 30) as $index) {

            $where = [
                'address_id'    => $faker->randomElement($addressIds->toArray()),
                'user_id'       => $faker->randomElement($userIds->toArray())
            ];

            $pivot = DB::table('address_user')->where($where)->first();

            if(empty($pivot)) {
                DB::table('address_user')->insert([
                    $where
                ]);
            }

        }

    }

}
