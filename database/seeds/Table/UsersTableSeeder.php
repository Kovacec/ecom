<?php

namespace Table;

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $localisedFaker = \Faker\Factory::create("sl_SI");

        // owner
        User::create([
            'name' => 'Bojan Kovacec',
            'email' => 'bojan.kovacec@gmail.com',
            'password' => '$2y$10$/VQy3hI9BMH1TRRU2crhT.vM.TALdzMI4CXPln9XUcMO015hR6Mvq',
            'street' => $localisedFaker->streetAddress(),
            'zip' => $localisedFaker->postcode(),
            'city' => $localisedFaker->city(),
            'latitude' => $localisedFaker->latitude(),
            'longitude' => $localisedFaker->longitude()
        ]);

        // test user
        User::create([
            'name' => 'Name Surname',
            'email' => 'test@example.com',
            'password' => '$2y$10$/VQy3hI9BMH1TRRU2crhT.vM.TALdzMI4CXPln9XUcMO015hR6Mvq',
            'street' => $localisedFaker->streetAddress(),
            'zip' => $localisedFaker->postcode(),
            'city' => $localisedFaker->city(),
            'latitude' => $localisedFaker->latitude(),
            'longitude' => $localisedFaker->longitude()
        ]);

        // test meber
        User::create([
            'name' => 'Name Surname',
            'email' => 'customer@example.com',
            'password' => '$2y$10$/VQy3hI9BMH1TRRU2crhT.vM.TALdzMI4CXPln9XUcMO015hR6Mvq',
            'street' => $localisedFaker->streetAddress(),
            'zip' => $localisedFaker->postcode(),
            'city' => $localisedFaker->city(),
            'latitude' => $localisedFaker->latitude(),
            'longitude' => $localisedFaker->longitude()
        ]);
    }
}
