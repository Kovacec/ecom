<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use MailThief\Testing\InteractsWithMail;

class RegistrationTest extends TestCase
{
    use InteractsWithMail;

    public function test_new_users_are_sent_a_welcome_email() {

        $this->post('register', [
           'name' => 'John Doe',
           'email' => 'john@example.com',
           'password' => 'secret'
        ]);

        // Check that an email was sent to this email address
        $this->seeMessageFor('john@exmaple.com');

        // BCC addresses are included as well
        $this->seeMessageFor('notifications@ekoloski-izdelki.si');

        // Make sure email has a correct Subject
        $this->seeMessageWithSubject('Dobrodošli na ekoloski-izdelki.si');

        //

    }
}

