<?php

namespace Tests\Unit;

use App\Cart;
use App\CartItem;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Item;

class StoreCartContent extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;

    public function test_can_store_cart_content()
    {
        $item1 = factory(Item::class)->create();
        $item2 = factory(Item::class)->create();
        $user = factory(User::class)->create();

        $cart = new Cart([
            'user_id' => $user->id
        ]);

        $cart->save();

        $data = (array_merge(
            $item2->cartRelevantAttributes(), ['qty' => rand(1, 10), 'cart_id' => $cart->id]
        ));
        // die(print_r());

        // die(print_r($data));
        $cartItem = factory(CartItem::class)->make();

        die($cartItem);

        // $cart->items()->create($data);

    }
}

